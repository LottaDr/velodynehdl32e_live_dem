from scipy.spatial.transform import Rotation as R
import numpy as np 
from pyproj import Transformer
from pyproj import CRS
from quat import *
import scipy.io as sio


def utc_to_weekseconds(utc,leapseconds):
    """ Returns the GPS week, the GPS day, and the seconds
        and microseconds since the beginning of the GPS week
        https://stackoverflow.com/questions/45422739/gps-time-in-weeks-since-epoch-in-python
    """
    import datetime, calendar
    datetimeformat = "%Y-%m-%d %H:%M:%S"
    epoch = datetime.datetime.strptime("1980-01-06 00:00:00",datetimeformat)
    tdiff = utc -epoch  + datetime.timedelta(seconds=leapseconds)
    gpsweek = tdiff.days // 7
    gpsdays = tdiff.days - 7*gpsweek
    gpsseconds = tdiff.seconds + 86400* (tdiff.days -7*gpsweek)
    return gpsweek, gpsdays, gpsseconds, tdiff.microseconds


class GprmcMessage():
    """
    :see https://de.wikipedia.org/wiki/NMEA_0183
    """
    def __init__(self, datetime=None, status=None, lat=None, lat_ori=None, long=None, long_ori=None, vel=None,
                 course=None, mag=None, mag_sign=None, singularity=None, weeks=None, seconds=None):
        self.datetime = datetime
        self.status = status
        self.lat = lat
        self.lat_ori = lat_ori
        self.long = long
        self.long_ori = long_ori
        self.velocity = vel
        self.course = course
        self.mag = mag
        self.mag_sign = mag_sign
        self.singularity = singularity
        self.weeks = None
        self.seconds = None

###calculate pitch yaw roll
##source: https://gist.github.com/MalcolmMielle/8e1ef4f0f9ffb15d5a0d9b746c1bc469

#obsolete functions; calculate without smoothing and prediction.
def convert_acc(acc):
    """Convert raw accelerometer measurements in roll pitch yaw
    https://stackoverflow.com/questions/3755059/3d-accelerometer-calculate-the-orientation
    The yaw equation comes from here https://robotics.stackexchange.com/questions/14305/yaw-from-accelerometer-no-so-what-do-these-equations-actually-mean
    If you have a magnetometer check out https://habr.com/en/post/499190/
    Args:
        acc (np.array): Array containing the three raw measurements on the accelerometer along
        x, y, z
    Returns:
        np.array: numpy array with [roll, pitch, yaw]
    """
    roll = math.atan2(acc[1], acc[2]) * (180 / math.pi)
    pitch = math.atan2(-acc[0], (math.sqrt((acc[1]*acc[1]) + (acc[2]*acc[2])))) * (180 / math.pi)
    yaw = math.atan(math.sqrt((acc[1]*acc[1]) +(acc[0]*acc[0])) / acc[2]) * (180 / math.pi)
    
    return np.array([roll, pitch, yaw])

def convert_gyro(orientation, gyro):
    """Convert raw gyroscope measurements in roll pitch yaw rate of change
    Args:
        orientation (np.array): Actual position of IMU as a numpy array containing [roll, pitch, yaw]
        gyro (np.array): Raw measurements of the gyroscope as a numpy array [Gx, Gy, Gz]
    Returns:
        np.array: numpy array with [roll, pitch, yaw] rates of change
    """
    # Transform gyro measurement into roll pitch yaw
    roll = orientation[0][0]  #phi
    pitch = orientation[1][0]  #theta
    
    # from http://www.chrobotics.com/library/understanding-euler-angles
    mat = np.array([[1, math.sin(roll) * math.tan(pitch), math.cos(roll) * math.tan(pitch)],
                    [0, math.cos(roll), -math.sin(roll)],
                    [0, math.sin(roll) / math.cos(pitch), math.cos(roll) / math.cos(pitch)]])
    
    rate_change_gyro = np.matmul(mat, gyro)
    return rate_change_gyro

#not in use; Complimentary filter implementation
def comp_fil(x,gyro,acc,dt):
    roll = 0.98*(x[0] + gyro[0]*dt) + 0.02*acc[0]
    pitch = 0.98*(x[1] + gyro[1]*dt) + 0.02*acc[1]
    yaw = 0.98*(x[2] + gyro[2]*dt) + 0.02*acc[2]

    return roll, pitch, yaw

#function to return roll,pitch,yaw rotation of coordinates
def rot_frame(self, y,p,r,x):
    rot = R.from_euler('xyz',[r,p,y])
    x_new = rot.apply(x)

    xn=x_new[0]
    yn=x_new[1]
    zn=x_new[2]

    return xn,yn,zn

#function to convert lat long coordinates from minute and second format to decimal
def gms_to_gdec(gms):
    g =int(gms/100)
    m =gms -g*100
    gdec = g+m/60
    #print(gdec)
    return gdec

#function tp transform wgs84 to ECEF 
def trans_coo(lat, lon):
    #define wgs84 as CRS EPSG:4326
    wgs84 = CRS.from_epsg(4326)
    #define earth centred cartesian coordinates as CRS EPSG:4978
    ccwgs = CRS.from_epsg(4978)

    #define transformation
    trans = Transformer.from_crs(4326,4978)
    x,y = trans.transform(lon,lat)

    return x,y

#get rotation matrix from given angles
def rotationMatrixToEulerAngles(R):
    sy = np.sqrt(R[0,0,:]*R[0, 0, :] + R[1, 0, :] * R[1, 0, :])
    x = np.arctan2(R[2, 1, :], R[2, 2, :])
    y = np.arctan2(-R[2, 0, :], sy)
    z = np.arctan2(R[1, 0, :], R[0, 0, :])
    return x, y, z

#function to rotate the point cloud
def rotFrame(self, ro,p,ya,x,y,z):
    xn = np.array([])
    yn = np.array([])
    zn = np.array([])
    #print(ro,p,ya)
    #Find time indexes relevant for this frame 
    #define rotation matrices with pitch yaw roll
    for j in np.arange(0,len(x)):
        rx = R.from_euler('x',ro,degrees =True)
        ry = R.from_euler('y',p, degrees =True)
        rz = R.from_euler('z',ya,degrees =True)
        r = rz*ry*rx
        new_co = r.apply([x[j],y[j],z[j]])
        xn = np.append(xn, new_co[0])
        yn = np.append(yn, new_co[1])
        zn = np.append(zn, new_co[2])

    return xn, yn, zn

#function to offset point cloud from position changes
def offsetFrame(self, tc,kx,ky,kvx, kvy,t,x,y, offx, offy):
    xn = np.array([])
    yn = np.array([])
    idx_low = 0
    idx_high= 0
    #define rotation matrices with pitch yaw roll
    for j in np.arange(0,len(x)):
        dt = t[j] - tc
        ox = x[j] + kx + (kvx * dt) - offx
        oy = y[j] + ky + (kvy * dt) - offy
        xn = np.append(xn, ox)
        yn = np.append(yn, oy)
    
    return xn, yn

#function of UKF oreintation estimation
def prediction(self, state, control, P, Q):
    qu = vec2quat(np.atleast_2d(control).T)

    S = np.linalg.cholesky(P+Q)
    n = P.shape[0]
    S = S*np.sqrt(0.5*n)
    W = np.array(np.hstack([S, -S]))

    noise_quaternions = vec2quat(W)
    X = multiply(noise_quaternions.T, state)

    Y = multiply(X, qu.T)

    next_state, error = q_mean(Y.T, state)

    next_cov = np.dot(error, error.T)/12.0

    return next_state, next_cov, Y, error

#can be used if bias correction is known; average over first 100 points, but can be hard coded with calibrated bias
def convert_measurements(self, acc, gyro, data_num):
    bias_g =np.array([np.mean(gyro[0,:100]),np.mean(gyro[1,:100]),np.mean(gyro[2,:100])])
    acc_bias = np.array([np.mean(acc[0,:100]),np.mean(acc[1,:100]),np.mean(acc[2,:100])])
    acc_corr = (acc - acc_bias) 

    gyro_corr = (gyro - bias_g)
    return acc_corr, gyro_corr

#UKF implementation of the orientation estimation
def estimate_rot(self, timestamp,gyro,acc,flag):
    # Data from dataset
    #imu = read_data_imu(data_num)
    #imu_np_data = np.array(imu['vals'], dtype=np.float64).T
    imu_ts = timestamp

    a_x = acc[0]
    a_y = acc[1]
    a_z = acc[2]
    acc = np.array([a_x, a_y, a_z]).T

    g_x = -gyro[2]#roll
    g_y = -gyro[0]#pitch
    g_z = -gyro[1]#yaw
    gyro = np.array([g_x, g_y, g_z]).T
    #acc_val, gyro_val = convert_measurements(acc, gyro, data_num)
    self.curr_state = np.array([1, 0, 0, 0])

    self.P = 0.00000000001 * np.identity(3)  # 0.00001
    self.Q = np.array([[0.003, 0, 0],
                [0, 0.003, 0],
                [0, 0, 0.003]])
    self.R = np.array([[0.005, 0, 0],
                [0, 0.005, 0],
                [0, 0, 0.005]])
    rpy = []

    #prev timestep solve with first unknown
    if flag:
        self.prev_timestep = imu_ts - 0.01 # set initial time int here

    

    self.control = gyro * (imu_ts - self.prev_timestep)
    self.prev_timestep = imu_ts


    self.curr_state, self.P, self.sigma_points, self.error = prediction(self, self.curr_state, self.control, self.P, self.Q)

    Z_simga_pts = multiply(quat_inv(self.sigma_points), np.array([0, 0, 0, 1]))
    Z_simga_pts = multiply(Z_simga_pts, self.sigma_points)
    Z_simga_pts = Z_simga_pts[:, 1:]

    z_est = np.mean(Z_simga_pts, axis=0)

    Z_error = (Z_simga_pts - z_est).T
    P_zz = np.dot(Z_error, Z_error.T) / 12.0
    P_vv = P_zz + self.R
    P_xz = np.dot(self.error, Z_error.T) / 12.0
    K = np.dot(P_xz, np.linalg.inv(P_vv))
    v = np.transpose(acc - z_est)
    Knu = vec2quat(np.atleast_2d(np.dot(K, v )).T)
    self.curr_state = multiply(Knu.T, self.curr_state).reshape(4, )
    self.P = self.P - np.dot(np.dot(K, P_vv), np.transpose(K))
    rpy.append(quat2rpy(self.curr_state))
    #else:
        # Atan
     #   rpy.append(rotmat2rpy(quat2rotmat(curr_state)))

    rpy = np.array(rpy)
    roll = np.array(rpy[0,0])
    pitch = np.array(rpy[0,1])
    yaw = np.array(rpy[0,2])

    return roll, pitch, yaw

#EKF Position estimation function
def kalman_xy(self, x, P, measurement, R,t,prev_time, flag,Q):
    if flag:
        x = np.array([measurement[0],measurement[1],0.,0.]).T
        P = np.array(np.eye(4))*1000
        print('init kalman states')
        self.k_init_x = measurement[0]
        self.k_init_y = measurement[1]
        return x,P
    else:
        dt= t - prev_time        
        return kalman(x, P, measurement, R, Q, prev_time,
                        F=np.array([[1.0, 0.0, dt, 0.0],
                                    [0.0, 1.0, 0.0, dt],
                                    [0.0, 0.0, 1.0, 0.0],
                                    [0.0, 0.0, 0.0, 1.0]]),
                        H=np.array([[1.0, 0.0, 0.0, 0.0],
                                    [0.0, 1.0, 0.0, 0.0]]))

#part of the EKF for position estimation
def kalman(x, P, measurement, R, Q,pre_t, F, H):

    y = np.array(measurement).T - np.dot(H,x)
    S = H.dot(P).dot(H.T) + R  # residual convariance
    K = np.dot((P.dot(H.T)), np.linalg.pinv(S))
    x = x + K.dot(y)
    I = np.array(np.eye(F.shape[0]))  # identity matrix
    P = np.dot((I - np.dot(K,H)),P)

    # PREDICT x, P
    x = np.dot(F,x)
    P = F.dot(P).dot(F.T) + Q

    return x, P

# correct heading
def cor_heading(lat, lon, p_lat, p_lon):
    A = np.cos(lon)*np.sin(lat-p_lat)
    B = np.cos(p_lon)*np.sin(lon)- np.sin(p_lon)*np.cos(lon)*np.cos(lat-p_lat)
    head =np.arctan2(A,B) 
    return head






