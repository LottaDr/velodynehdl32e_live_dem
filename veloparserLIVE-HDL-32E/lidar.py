import os
from datetime import datetime
import argparse
import dpkt
import math
import struct
import numpy as np
from scapy.all import *
import binascii

from gps import *

#functions to decode data from packets
def read_uint8(data, idx):
  return data[idx]


def read_sint8(data, idx):
  val = read_uint8(data, idx)
  return val-256 if val > 127 else val


def read_uint16(data, idx):
  return data[idx] + data[idx+1]*256


def read_sint16(data, idx):
  val = read_uint16(data, idx)
  return val-2**16 if val > 2**15-1 else val


def read_uint32(data, idx):
  return data[idx] + data[idx+1]*256 + data[idx+2]*256*256 + data[idx+3]*256*256*256

#read of gyro and acc values with shiftes hexadecimal scale
def read_gyro(data):
    gyro =bytearray(data)
    gd_int = int.from_bytes(gyro[1:], byteorder='big')
    #reduce positional data byte to 0 
    while gd_int >15:
        gd_int = gd_int-16
    gyro[1:] = (gd_int).to_bytes(1, byteorder='big')
    #now compute int value for twos complement bin
    gyro_i=int.from_bytes(gyro,'little')
    if gyro_i >=2048:
        gyro_i -= 2**(12)
    return gyro_i

#Class of the Lidar application with defintion of the two processes for data and position packets
class Lidar():
    def __init__(self):
        self.point_cloud = []

    def process_data_frame(self, data, frame_idx):
        raise NotImplementedError("Subclasses should implement this!")

    def process_position_frame(self, data, frame_idx):
        raise NotImplementedError("Subclasses should implement this!")

#define object class of the lidar sensor with all needed parameters needed for the implementation
class VelodyneHDL32E(Lidar):
    # factor distance centimeter value to meter
    FACTOR_CM2M = 0.01

    # factor distance value to cm, each velodyne distance unit is 2 mm
    FACTOR_MM2CM = 0.2

    def __init__(self, dual_mode=False):
        super(VelodyneHDL32E, self).__init__()
        self.dual_mode = dual_mode
        self.timing_offsets = self.calc_timing_offsets()

        self.omega = np.array([-30.67,-9.33,-29.33,-8.00,-28.00,-6.67,-26.67,-5.33,-25.33,-4.0,-24.00,-2.67,-22.67,-1.33,-21.33,0.00,-20.00,1.33,-18.67,2.67,-17.33,4.00,-16.0,5.33,-14.67,6.67,-13.33,8.00,-12.00,9.33,-10.67,10.67])
        self.count_lasers = 32
        #kalman parameter für lidar system; roll pitch yaw
        self.k_pitch = None
        self.k_roll = None
        self.k_yaw = None
        self.k_init_x = None 
        self.k_time = None
        self.k_init_y = None
        self.k_x = None 
        self.k_y = None 
        self.k_vx = None 
        self.k_vy = None  
        self.heading = None
        self.long_lat_hist = np.zeros([2,20])
        self.lat = 0.
        self.lon = 0.
        self.hist_count = 0
        self.prev_timestep = None
        self.control = None
        self.P = None
        self.Q = None
        self.R = None 
        self.curr_state = None
        self.state  = None
        self.next_cov = None
        self.next_state = None
        self.Y  = None
        self.sigma_points = None 
        self.error = None

        #kalman parameter für GPS von lidar continous fit
        self.pre_t = None 
        self.x = None 
        self.P_coo = None 
        self.R_coo = None 
        self.Q_coo = np.array(np.eye(4))

    #calculate timing offsets of each measurment from apcket timestamp
    def calc_timing_offsets(self):
        timing_offsets = [[0.0 for x in range(12)] for y in range(32)]  # Init matrix

        # constants
        full_firing_cycle = 46.08  # μs
        single_firing = 1.152  # μs
        # compute timing offsets
        for x in range(12):
            for y in range(32):
                if self.dual_mode:
                    dataBlockIndex = (x - (x % 2)) + int((y / 32))
                else:
                    dataBlockIndex = (x * 2) + int((y / 32))
                dataPointIndex = y % 32
                timing_offsets[y][x] = \
                    (full_firing_cycle * dataBlockIndex) + (single_firing * dataPointIndex)
        return np.array(timing_offsets).T

    #read data from packet and process into orientation corrected, georeferenced point cloud
    def process_data_frame(self, params, data, frame_idx,flag_init):
        """
        :param data: A velodyne packet consisting of 12 (n) blocks and 24 (m) sequences and 16 firing pre sequence
        :param frame_idx:
        :return: X,Y,Z-coordinate, azimuth, intensitiy, timestamp of each firing, sequence ordered, shape of each = [384x1]
        """
        data=np.frombuffer(data,dtype=np.uint8).astype(np.uint32)
        
        timestamp = read_uint32(data, 1200)
        factory = data[1204:]

        blocks = data[0:1200].astype(int).reshape(12, 100)

        distances = []
        intensities = []
        azimuth_per_block = []
        # iteratie through each block
        for i, blk in enumerate(blocks):
            dists, intens, angles = self.read_firing_data(blk)
            distances.append(dists)
            intensities.append(intens)
            azimuth_per_block.append(angles)


        azimuth_per_block = np.array(azimuth_per_block)

        ## Note: all these array have th same size, number of firing in one packet
        azimuth = self.calc_precise_azimuth(azimuth_per_block).reshape(12, 32)
        distances = np.array(distances)
        intensities = np.array(intensities)

        # now calculate the cartesian coordinate of each point
        X, Y, Z = self.calc_cart_coord(distances, azimuth)

        #rotate accoring to sensor orientation (initial)
        x = Z
        z = -X

        # calculating timestamp [microsec] of each firing
        timestamps = timestamp + self.timing_offsets

        X = x.flatten()
        Y = Y.flatten()
        Z = z.flatten()
        intensities = intensities.flatten()
        azimuth = azimuth.flatten()
        timestamps = timestamps.flatten()
        distances = distances.flatten() * self.FACTOR_MM2CM * self.FACTOR_CM2M

        # remeber the last azimuth for roll over checking
        self.cur_azimuth = azimuth

        #adjust xyz with current position offset and rpy from kalman filters
        #print(flag_init)
        if self.k_roll == None :
           print('Orientation of Frame not defined yet')
        else:
            if params['ori_cor']:
                X, Y, Z = rotFrame(self, self.k_roll,self.k_pitch,self.k_yaw,X, Y,Z) 
            #calc offset through position change next
            if params['pos_cor']:
                #correct heading to align north
                self.head = cor_heading(self.lat,self.lon,self.long_lat_hist[1,(self.hist_count+1)%20], self.long_lat_hist[0,(self.hist_count+1)%20])
                X,Y,Z = rotFrame(self, 0,0,self.head,X, Y, Z)
                X,Y = offsetFrame(self, self.prev_timestep,self.k_y, self.k_x,self.k_vy,self.k_vx,timestamps,X,Y,self.k_init_y,self.k_init_x)


        return X, Y, Z, intensities, azimuth, timestamps, distances
    #process of reading the position package, estimate orientation  and position changes and save kalman parameter and imu, gps, data
    def process_position_frame(self,params, data, frame_idx, flag_init):
        timestamp = data[198:202]
        timestamp = read_uint32(timestamp, 0)

         ##read gyro data from message and use scale factors to produce processed values

        gyro1 = read_gyro(data[14:16])*0.09766 # degree/sec
        gyro2 = read_gyro(data[22:24])*0.09766 # degree/sec
        gyro3 = read_gyro(data[30:32])*0.09766 # degree/sec
        gyro = np.array([gyro1, gyro2, gyro3])

        temp1 = read_gyro(data[16:18])*0.1453 + 25 #degree celsius
        temp2 = read_gyro(data[24:26])*0.1453 + 25 #degree celsius
        temp3 = read_gyro(data[32:34])*0.1453 + 25 #degree celsius

        acx1 = read_gyro(data[18:20])*0.001221 #G
        acx2 = read_gyro(data[26:28])*0.001221 #G
        acx3 = read_gyro(data[34:36])*0.001221 #G

        acy1 = read_gyro(data[20:22])*0.001221 #G
        acy2 = read_gyro(data[28:30])*0.001221 #G
        acy3 = read_gyro(data[36:38])*0.001221 #G

        acc = np.array([acy1,acy2,acx1])

        imu_data = [gyro1,gyro2,gyro3,temp1,temp2,temp3,acx1,acx2,acx3,acy1,acy2,acy3]


        ppps_status = data[202]

        gprmc = data[206:]
        gprmc = gprmc.split()[0]  # filter out gprmc message, remaining are zeros
        gprmc = gprmc.decode('ascii').split(',')  # convert bytes array to string

        gps_msg = GprmcMessage()
        time = gprmc[1]
        date = gprmc[9]
        gps_msg.datetime = datetime.strptime(date + time, '%d%m%y%H%M%S')

        gps_msg.status = gprmc[2]
        gps_msg.lat = float(gprmc[3])
        gps_msg.lat_ori = gprmc[4]
        gps_msg.long = float(gprmc[5])
        gps_msg.long_ori = gprmc[6]
        if gprmc[7]=='':
            gps_msg.velocity=0.
        else: 
            gps_msg.velocity = float(gprmc[7])
        if gprmc[8]=='':
            gps_msg.course=0.
        else: 
            gps_msg.course = float(gprmc[8])

        gps_msg.mag = float(gprmc[10])
        gps_msg.mag_sign = gprmc[11]
        gps_msg.singularity = gprmc[12]
        gps_msg.weeks, _, gps_msg.seconds, _ = utc_to_weekseconds(gps_msg.datetime, leapseconds=0)

        #estimate rpy and position with new kalman parametern for predicition
        if params['ori_cor']:
            k_roll, k_pitch , k_yaw = estimate_rot(self, timestamp,gyro, acc,flag_init)
        #print(k_roll,k_pitch,k_yaw)
        #estimate current position using kalman filter
        #tansform coordinates
        dec_lat = gms_to_gdec(gps_msg.lat)
        dec_lon = gms_to_gdec(gps_msg.long)

        #if latitude longitude from external INS change self.lat self.lon here, so they can be used in kalman estimation
        self.lat = dec_lat
        self.lon = dec_lon
        #change from wgs84 to earth centred coordinates 
        lat, lon = trans_coo(dec_lat,dec_lon)    
        meas= np.array([lat,lon])
        #calculate kalman 
        if params['pos_cor']:
            self.long_lat_hist[0,self.hist_count] = dec_lon
            self.long_lat_hist[0,self.hist_count] = dec_lat
            self.hist_count = (self.hist_count + 1)%20  
            #print(self.hist_count) 
            self.x, self.P_coo = kalman_xy(self, self.x, self.P_coo,meas,0.01**2,timestamp,self.prev_timestep, flag_init,self.Q_coo)
        #print(self.x)

        #if rpy input from other message check time delay and perform matching if necessary. This program uses a PCAP file with packets arriving in order
        #input rpy here instead of the kalman_para variable
        if params['pos_cor'] & params['ori_cor']:
            kalman_para = np.array([timestamp, k_roll,k_pitch,k_yaw,self.x[0],self.x[1],self.x[2],self.x[3]])
            self.k_roll = kalman_para[1]
            self.k_yaw = kalman_para[3]
            self.k_pitch = kalman_para[2]
            self.k_time = kalman_para[0]
            self.k_x = kalman_para[4]
            self.k_y = kalman_para[5]
            self.k_vx = kalman_para[6]
            self.k_vy = kalman_para[7]
        elif params['pos_cor'] & (not params['ori_cor']):
            kalman_para = np.array([timestamp,self.x[0],self.x[1],self.x[2],self.x[3]])
            self.k_time = kalman_para[0]
            self.k_x = kalman_para[1]
            self.k_y = kalman_para[2]
            self.k_vx = kalman_para[3]
            self.k_vy = kalman_para[4]
        elif params['ori_cor'] & (not params['pos_cor']):
            kalman_para = np.array([timestamp, k_roll,k_pitch,k_yaw])
            self.k_roll = kalman_para[1]
            self.k_yaw = kalman_para[3]
            self.k_pitch = kalman_para[2]
            self.k_time = kalman_para[0]
               

        return gps_msg, imu_data, timestamp, kalman_para

    #function to calculate precise azimuth from data packet position of measurement
    def calc_precise_azimuth(self, azimuth):
        """
        Linear interpolation of azimuth values
        :param azimuth:
        :return:
        """
        org_azi = azimuth.copy()

        precision_azimuth = []
        # iterate through each block
        for n in range(12): # n=0..11
            azimuth = org_azi.copy()
            try:
                # First, adjust for an Azimuth rollover from 359.99° to 0°
                if azimuth[n + 1] < azimuth[n]:
                    azimuth[n + 1] += 360.

                # Determine the azimuth Gap between data blocks
                azimuth_gap = azimuth[n + 1] - azimuth[n]
            except:
                azimuth_gap = azimuth[n] - azimuth[n-1]

            factor = azimuth_gap / 32.
            k = np.arange(32)
            precise_azimuth = azimuth[n] + factor * k
            precision_azimuth.append(precise_azimuth)
        precision_azimuth = np.array(precision_azimuth)
        return precision_azimuth

    def calc_precise_azimuth_2(self, azimuth):
        org_azi = azimuth.copy()

        precision_azimuth = []
        # iterate through each block
        for n in range(12): # n=0..11
            azimuth = org_azi.copy()
            try:
                # First, adjust for an Azimuth rollover from 359.99° to 0°
                if azimuth[n + 1] < azimuth[n]:
                    azimuth[n + 1] += 360.

                # Determine the azimuth Gap between data blocks
                azimuth_gap = azimuth[n + 1] - azimuth[n]
            except:
                azimuth_gap = azimuth[n] - azimuth[n-1]

            # iterate through each firing
            for k in range(32):
                # Determine if you’re in the first or second firing sequence of the data block
                if k < 16:
                    # Interpolate
                    precise_azimuth = azimuth[n] + (azimuth_gap * 1.152 * k) / 46.08
                else:
                    # interpolate
                    precise_azimuth = azimuth[n] + (azimuth_gap * 1.152 * ((k-32) + 46.08)) / 46.08
                if precise_azimuth > 361.:
                    print("Error")
                print(precise_azimuth)
                precision_azimuth.append(precise_azimuth)
        precision_azimuth = np.array(precision_azimuth)
        return precision_azimuth
    #function to read in the data from data packet
    def read_firing_data(self, data):
        block_id = data[0] + data[1]*256
        # 0xeeff is upper block
        assert block_id == 0xeeff
        azimuth = (data[2] + data[3] * 256) / 100
        firings = data[4:].reshape(32, 3)
        distances = firings[:, 0] + firings[:, 1] * 256
        intensities = firings[:, 2]
        return distances, intensities, azimuth
    #function to go from measured spherical coordinates to xyz
    def calc_cart_coord(self, distances, azimuth):
        # convert distances to meters
        distances = distances * self.FACTOR_MM2CM * self.FACTOR_CM2M

        # convert deg to rad
        #longitudes = np.tile(self.omega * np.pi / 180., 2)
        longitudes=self.omega*np.pi/180.
        latitudes = azimuth * np.pi / 180.

        hypotenuses = distances * np.cos(longitudes)
        X = hypotenuses * np.sin(latitudes)
        Y = hypotenuses * np.cos(latitudes)
        Z = distances * np.sin(longitudes)
        return X, Y, Z
