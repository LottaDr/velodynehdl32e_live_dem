import pandas as pd
import os 
import numpy as np
import matplotlib.pyplot as plt

np.set_printoptions(suppress=True)

#assign directory
#file ='/Users/LottaDraeger/Desktop/testing/58_frame_295341.559512.txt'
file ='/Users/LottaDraeger/Desktop/testing/222_frame_491260.352677.txt'
directory ='/Users/LottaDraeger/Desktop/LiDAR_Live/Tests on grass and Street/test1/velodynehdl32e/data_ascii'

#initialze flag counter to see how many tracks are skipped with no info about ice surface over all tracks
flag=0
#initiallize output arrays
ts_beg = np.array([])
ts_end = np.array([])
track_l = np.array([])
hight_m = np.array([])
hight_rms = np.array([])
id_track = np.array([])
filtered = np.array([])
results =np.empty([0,6])

#iterate over txt results file in this directory 
os.chdir(directory)
for filename in os.listdir():
	if filename.endswith('.txt'):
		print('Reading file: ', filename)

		#reading in the data into pandas data frame
		data =pd.read_csv(filename, sep=',')

			
		#compute roughness for each track ID and save roughness parameter, track ID, track length and timestamp to file. 
		#for computation of the roughness see which azimuth angles record the ice/water surface and which record boat and sky
		#this is dependant on the data campaign and inferred from the given data. 
			
		#filter around mean height to remove outliers
		#test_fil = data[(data[" Latitude [Deg]"]>= 270.0) & (data[" Latitude [Deg]"]<= 300.0)]
		test_fil = data[(data[" Latitude [Deg]"]>= 270.0)]
		#test_fil = data
		
		#if filename == "110_frame_299161.228426.txt":
		#fig=plt.figure()
		#ax=fig.add_subplot(projection='3d')
		#ax.scatter(test_fil[" X [m]"],test_fil[" Y [m]"],test_fil[" Z [m]"])
		#ax.set_title('Examplary data frame over grass with 90° FOV', pad=20)
		#ax.set_xlabel('x direction [m]')
		#ax.set_ylabel('y direction [m]')
		#ax.set_zlabel('z direction [m]')
		#ax.scatter(data[" X [m]"],data[" Y [m]"],data[" Z [m]"])
		#plt.plot(track[" X [m]"],track[" Y [m]"],'ob' )
			
		plt.show()

		#now sort data into frames
		for i in range(0,32):
				#track for velodynehdl32e data
				#track = data[(data[" ID"]==i) & (data[" Distance [m]"] != 0) & (data[" Latitude [Deg]"]>= 270.0) & (data[" Latitude [Deg]"]<= 300.0)]
				track = data[(data[" ID"]==i) & (data[" Distance [m]"] != 0) & (data[" Latitude [Deg]"]>= 270.0)]
				#print(data[(data[" ID"]==i) & (data[" X [m]"]<= -3) & (data[" Distance [m]"] != 0)])
				track.reset_index(drop=True, inplace=True)
				#if (i==16):
				#if (i==16) & (filename =="35_frame_295318.859006.txt"):
				#	fig=plt.figure()
				#	ax=fig.add_subplot(projection='3d')
				#	ax.scatter(track[" X [m]"],track[" Y [m]"],track[" Z [m]"])
				#	ax.set_title('Central track over grass with 90° FOV', pad=20)
				#	ax.set_xlabel('x direction [m]')
				#	ax.set_ylabel('y direction [m]')
				#	ax.set_zlabel('z direction [m]')
					#ax.scatter(data[" X [m]"],data[" Y [m]"],data[" Z [m]"])
					#plt.plot(track[" X [m]"],track[" Y [m]"],'ob' )
						
				#	plt.show()
				#compute track length, array ordered in ascending azimuth so first and last entry are farthest apart
				if track.shape[0]>1:
					#filter the data
					#fil_mean =np.mean(track[" Z [m]"])
					#fil_std = np.std(track[" Z [m]"])
					#print(fil_mean,fil_std, track[" Z [m]"])
					#fil_track = track[(track[" Z [m]"]>= (fil_mean-fil_std)) & (track[" Z [m]"] <= (fil_mean+fil_std))]
					#print(track.shape[0]-fil_track.shape[0])
					#mean= np.mean(fil_track[" Z [m]"])
					#print(fil_mean, mean)
					#filtered = np.append(filtered,track.shape[0] - fil_track.shape[0])
					#set filtered track as track
					#old_track=track
					#track =fil_track
					#if (i==16):
					
					# if (i==16) & (filename =="35_frame_295318.859006.txt"):
					#	fig=plt.figure()
					#	ax=fig.add_subplot(projection='3d')
					#	ax.scatter(track[" X [m]"],track[" Y [m]"],track[" Z [m]"], 'ob', label='filtered data points',zorder=2)
					#	ax.scatter(old_track[" X [m]"],old_track[" Y [m]"],old_track[" Z [m]"], 'or', label='unfiltered data points',zorder=-1)
					#	ax.set_title('Examplary filtered data for the central track measured over grass', pad=20)
						#ax.set_xlabel('x direction [m]')
						#ax.set_ylabel('y direction [m]')
						#ax.set_zlabel('z direction [m]')
						#ax.scatter(data[" X [m]"],data[" Y [m]"],data[" Z [m]"])
						#plt.plot(track[" X [m]"],track[" Y [m]"],'ob' )
							
					#	plt.show()

					#	plt.plot(old_track[" X [m]"],old_track[" Y [m]"])
					#	plt.title('Track projection in x-y plane')
					#	plt.xlabel('x direction [m]')
					#	plt.ylabel('y direction [m]')
					#	plt.show()

					#	plt.plot(old_track[" X [m]"],old_track[" Z [m]"])
					#	plt.title('Track projection in x-z plane')
					##	plt.xlabel('x direction [m]')
					#	plt.ylabel('z direction [m]')
					#	plt.show()

					#	plt.plot(old_track[" Y [m]"],old_track[" Z [m]"])
					#	plt.title('Track projection in y-z plane')
					#	plt.xlabel('y direction [m]')
					#	plt.ylabel('z direction [m]')
					#	plt.show()



					#timestamp of beginning and ending of the track measurement
					#ts_beg = np.append(ts_beg,track.iloc[0,0].astype(int))
					#ts_end = np.append(ts_end,track.iloc[-1,0].astype(int))

					#compute the track length in 3D
					tr_length = np.sqrt((track.iloc[-1,1]-track.iloc[0,1])**2+(track.iloc[-1,2]-track.iloc[0,2])**2+(track.iloc[-1,3]-track.iloc[0,3])**2)
					

					#fit line in 3d to get trend; 
					#try1 with x,y,z independent
					#fit =track[[" X [m]"," Y [m]"," Z [m]"]].to_numpy()

					#calc mean of point cloud
					#datamean = fit.mean(axis=0)

					# Do an SVD on the mean-centered data.
					#uu, dd, vv = np.linalg.svd(fit - datamean)

					#linepts = vv[0] * np.mgrid[-7:7:2j][:, np.newaxis]

					# shift by the mean to get the line in the right place
					#linepts += datamean

					# Verify that everything looks right.

					#	fig = plt.figure()
					#	ax=fig.add_subplot(projection='3d')
					#	ax.scatter(*fit.T, 'Measurement points')
					#	ax.plot(*linepts.T, label='Least Squares solution')
					#	ax.set_title('Least Squares fit for an examplary track over paved street')
						#ax
					#	plt.show()

					#with z= f(x,y)
					fit =track[[" X [m]"," Y [m]"," Z [m]"]].to_numpy()
					A = np.c_[fit[:,0],fit[:,1],np.ones(fit.shape[0])]
					C,e,_,_ =np.linalg.lstsq(A,fit[:,2],rcond=None)

					#look that it is alright
					#create continous x and y samples
					x=np.linspace(min(fit[:,0]),max(fit[:,0]),num=20, endpoint=True)
					y=np.linspace(min(fit[:,1]),max(fit[:,1]),num=20, endpoint=True)

					z=C[0]*x + C[1]*y + C[2]
					#print(e)
					#if (i==16):
					#if (i==16) & (filename =="35_frame_295318.859006.txt"):
					#	fig = plt.figure()
					#	ax=fig.add_subplot(projection='3d')
					#	ax.plot(x,y,z,'-r', label='Least Squares Solution',zorder =10)
					#	ax.scatter(*fit.T, label='Measurement Data', zorder=-1)
					#	#ax.plot(x,y,z,'-r', label='Least Squares Solution',zorder =2)
					#	ax.set_title('Least Squares fit for an examplary track over grass')
					#	ax.set_xlabel('x direction [m]')
					#	ax.set_ylabel('y direction [m]')
					#	ax.set_zlabel('z direction [m]')
					#	plt.legend()
					#	plt.show()

					#reduce trend in z from the data
					#print(datamean + vv[0,2]*track[" Z [m]"])
					#gradz=vv[0,2]
					#if (i==16):
					#if (i==16) & (filename =="35_frame_295318.859006.txt"):
					#	fig=plt.figure()
					#	ax=fig.add_subplot(projection='3d')
					#	ax.scatter(track[" X [m]"],track[" Y [m]"],track[" Z [m]"],'or', label ='Measured data points')
					##	z_new = track.loc[:," Z [m]"] = track.loc[:," Z [m]"] - (C[0]*track.loc[:," X [m]"] + C[1]*track.loc[:," Y [m]"] + C[2])
					#	ax.scatter(track[" X [m]"],track[" Y [m]"],z_new, 'ob',label='Detrended data points')
					#	ax.set_title('Detrended and original Data of examplary track over grass')
					#	ax.set_xlabel('x direction [m]')
					#	ax.set_ylabel('y direction [m]')
					#	ax.set_zlabel('z direction [m]')
					#	plt.show()


					#	plt.plot(fit[:,0],fit[:,2],'-r',label='Original height profile')
					#	plt.plot(fit[:,0],z_new,'-b',label='Trend corrected height profile')
					#	plt.title('Height profile projected along X direction')
					#	plt.legend()
					#	plt.xlabel('x direction [m]')
					#	plt.ylabel('Height of surface [m]')
					#	plt.show()




					#plt.scatter(track[" X [m]"], track[" Z [m]"])
					#plt.show()

					#print(tr_length)
					#append result to array
					#filter for very long and very short profiles : more than 4m or less than 1m
					if (tr_length >=1) & (tr_length <= 4):
						#filtered = np.append(filtered,old_track.shape[0] - fil_track.shape[0])
						#timestamp of beginning and ending of the track measurement
						ts_beg = np.append(ts_beg,track.iloc[0,0].astype(int))
						ts_end = np.append(ts_end,track.iloc[-1,0].astype(int))
						track_l = np.append(track_l,tr_length)
						#compute mean hight of track
						#hight= track[" Z [m]"].abs()
						hight= track[" Z [m]"]
						h_mean = hight.sum() / track.shape[0]
						#append mean hight of track
						hight_m = np.append(hight_m,h_mean)
						#compute rms hight deviation
						dev= (hight - h_mean)**2
						rms = np.sqrt(dev.sum()/ track.shape[0])
						#append hight rms of track
						hight_rms = np.append(hight_rms, rms)
						id_track = np.append(id_track,i)
						if (i==16):
						#if (i==16) & (filename =="35_frame_295318.859006.txt"):
							print(tr_length)
							print(ts_beg, ts_end)
							print(h_mean)
							print(rms)
				else: 
					flag += 1
					#print("Indexing error,something went wrong. Shape is: ", track.shape)

	print(flag)
	#print(ts_beg.shape, ts_end.shape, track_l.shape, hight_m.shape, hight_rms.shape)
	#write output file 

	#print(max(hight_rms),min(hight_rms),np.mean(hight_rms),np.std(hight_rms),np.mean(track_l))
	out_arr=np.column_stack((ts_beg,ts_end,id_track,track_l,hight_m,hight_rms))
	results = np.vstack([results,out_arr])

	fmt=('%d','%d','%d','%4.5f','%4.5f','%2.7f')
	np.savetxt("test1.txt", out_arr, delimiter=", ", header="Timestamp begin [ms], Timestamp end [ms], Track ID, Track length [m], Mean hight [m], RMS Hight [m]",fmt=fmt)
print(np.shape(results))
print (np.mean(results[:,3]),min(results[:,3]),max(results[:,3]),np.std(results[:,3]),np.mean(results[:,5]),np.std(results[:,5]))

			##############BEFORE PROCEEDING ##################
			# Find out how to filter an overall trend in the xyz coordinates before taking RMS for the roughness
				#done with linear regression using z=f(x,y)
					##calculate length of scan line, mean height of track, and the standard deviation

			#for track length find min and max azimuth and then compute length of xyz coordinates
			
			#Pancake low: Consider data with X <= -3m as this excludes all data points reflected from the boat in across configuration
			#VelodyneHDL32 Data: Consider data with X<= -1.5
			#Grey Ice Cakes Across: Consider data with X <= -11m
			#Gry Ice Along Nadir:Consider data with Y <=-10
			#Grey Ice Cakes Along Slant: Consider data with Y<=-10
			#Pack Ice Slant:Y <=-9.5
			#Pack Ice Across: X<=-1





