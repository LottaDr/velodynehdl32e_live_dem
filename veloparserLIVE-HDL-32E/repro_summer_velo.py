#script to reprocess data from Summer 2022 Polarstern application 
#implementation had still some mistakes which are reversed here as best as possible

import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
from quat import *
import scipy.io as sio
import time
import os
from scipy.spatial.transform import Rotation as R
from datetime import datetime
import open3d as o3d

int_time_flag = False

#location of gps file
filename = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/data_gps.txt'
#latitude longitude Data
if int_time_flag:
	data = pd.read_csv(filename,sep=',', encoding ='latin1')
else:
	data = pd.read_csv(filename,sep=',', encoding='latin1')
	data = data.groupby(by=['# UTC-Time']).first().reset_index()

#data read in
lat=data[' Latitude [Deg]'].to_numpy(dtype =str)
lon= data[' Longitudes [Deg]'].to_numpy(dtype =str)

new = data[' Latitude [Deg]'].str.split(" ",expand = True)
data[' Latitude [Deg]'] = new[1]
data[' N/S'] = new[2]
new = data[' Longitudes [Deg]'].str.split(" ",expand = True)
data[' Longitudes [Deg]'] = new[1]
data[' W/E'] = np.full((len(new[1]),1),' W')
#print(data[' Longitudes [Deg]'])

#Accelerometer data 
accx= data[' Acc Y1 [G]'].to_numpy()
accy= data[' Acc Y2 [G]'].to_numpy()
accz= - data[' Acc X1 [G]'].to_numpy()
#Gyro data
gyro1 = data[' Gyro1 [deg/sec]'].to_numpy()
gyro2 = data[' Gyro2 [deg/sec]'].to_numpy()
gyro3 = data[' Gyro3 [deg/sec]'].to_numpy()

#calculate musec timestamp from utc time saved during measurment
timestamp = np.array([])
if int_time_flag: 
	#timestamps
	time_utc = data['# UTC-Time'].to_numpy(str)
	sort = data.groupby(by=['# UTC-Time']).size().reset_index(name='rows')
	n_rows = sort['rows'].to_numpy()

	#calculate timestamps as toh in ms and interpolate not saved ms times by evenly spacing measurements for each second intervall 

	j=0
	i=0

	print(len(time_utc))
	while i < len(time_utc):
		count=0
		while count < n_rows[j]:
			timestamp_obj = datetime.strptime(time_utc[i], '%Y-%m-%d %H:%M:%S')
			time_toh = (timestamp_obj.minute*60000000+timestamp_obj.second*1000000+((n_rows[j]/1000000)*count*1000000))
			count +=1
			i+=1
			timestamp = np.append(timestamp,time_toh)
			print(i,j,count, n_rows[j])
		j+=1

	print(timestamp)

	data[' Timestamp'] = timestamp
else:
	time_utc = data['# UTC-Time'].to_numpy(str)
	for i in range(0,len(time_utc)):
		timestamp_obj = datetime.strptime(time_utc[i], '%Y-%m-%d %H:%M:%S')
		time_toh = (timestamp_obj.minute*60000000+timestamp_obj.second*1000000)
		timestamp = np.append(timestamp,time_toh)

	data[' Timestamp'] = timestamp

#reverse engineer gyro and acc data
#scaling factor
gyro1 = gyro1/0.09766
gyro2 = gyro2/0.09766
gyro3 = gyro3/0.09766

accx = accx/0.001221
accy = accy/0.001221
accz = accz/0.001221

for i in range(0,len(gyro1)):
	print(gyro1[i],int(gyro1[i]))
	if int(gyro1[i]) >=2048:
		gyro1[i] -= 2**(12)
	if int(gyro2[i]) >=2048:
		gyro2[i] -= 2**(12)
	if int(gyro3[i]) >=2048:
		gyro3[i] -= 2**(12)
	if int(accx[i]) >=2048:
		accx[i] -= 2**(12)
	if int(accy[i]) >=2048:
		accy[i] -= 2**(12)
	if int(accz[i]) >=2048:
		accz[i] -= 2**(12)

#reapply scaling factor
gyro1 = gyro1*0.09766
gyro2 = gyro2*0.09766
gyro3 = gyro3*0.09766

accx = accx*0.001221
accy = accy*0.001221
accz = accz*0.001221

#data.drop(columns = [' Acc Y1 [G]',' Acc Y2 [G]' ])

data[' Acc Y1 [G]'] = accx
data[' Acc Y2 [G]'] = accy
data[' Acc X1 [G]'] = accz
#Gyro data
data[' Gyro1 [deg/sec]'] = gyro1
data[' Gyro2 [deg/sec]'] = gyro2
data[' Gyro3 [deg/sec]'] = gyro3

data.to_csv('/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/gps_corr_slice.dat')




