import numpy as np 
from pyproj import Transformer
from pyproj import CRS
import pandas as pd
import matplotlib.pyplot as plt
from quat import *
import scipy.io as sio
import time
import os
from scipy.spatial.transform import Rotation as R
from datetime import datetime
import open3d as o3d


#setup Mesurements first
#GPS from Lat Long in WGS4 to XYZ Earth Bound
#define wgs84 as CRS EPSG:4326
wgs84 = CRS.from_epsg(4326)
#define earth centred cartesian coordinates as CRS EPSG:4978
ccwgs = CRS.from_epsg(4978)

#define transformation
trans = Transformer.from_crs(4326,4978)

#function to convert geodetic minutes and seconds into degree
def gms_to_gdec(gms):
	g =int(gms/100)
	m =gms -g*100
	gdec = g+m/60
	#print(gdec)
	return gdec

#input GPS data from file
lat=np.array([]) #4 
lon=np.array([]) #5

#filename='/Volumes/LOTTA/Master/Ergebnisse/arctic_gps/velodynehdl32e/data_gps.txt'
#filename= '/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/GIT Abgabe/velodynehdl32e_live_dem/testing/offline/resultt/gps_slice.dat'
filename = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/gps_corr_slice.dat'
data = pd.read_csv(filename,sep=',')
#data = data.iloc[:4247]
#latitude longitude Data
lat=data[' Latitude [Deg]'].to_numpy()
lon= data[' Longitudes [Deg]'].to_numpy()

#Accelerometer data 
accx= data[' Acc Y1 [G]'].to_numpy()
accy= data[' Acc Y2 [G]'].to_numpy()
accz= data[' Acc X1 [G]'].to_numpy()


#Gyro data
gyro1 = data[' Gyro1 [deg/sec]'].to_numpy()
gyro2 = data[' Gyro2 [deg/sec]'].to_numpy()
gyro3 = data[' Gyro3 [deg/sec]'].to_numpy()

#timestamps
timestamp = data[' Timestamp'].to_numpy()

#convert to degree
dec_lat = np.array([])
dec_lon = np.array([])
for i in np.arange(0,len(lat)):
	#print(i)
	dec_lat = np.append(dec_lat,gms_to_gdec(lat[i]))
	dec_lon = np.append(dec_lon,gms_to_gdec(lon[i]))
#print(dec_lat,dec_lon)
#convert from wgs 84 to earthcentred cartesian
ec_x=np.array([])
ec_y=np.array([])

for i in np.arange(0,len(dec_lon)):
	xp,yp = trans.transform(dec_lon[i],dec_lat[i])
	ec_x=np.append(ec_x,xp)
	ec_y=np.append(ec_y,yp)

def smooth_data(data,ts, n_samples):
	#smooth data using n sample average and set new time to the middle of the averaged time step
	result = np.array([])
	filter_time =np.array([])
	for i in range(len(data)- n_samples):
		for _ in range(n_samples):
			result = np.append(result, np.mean(data[i:i+n_samples]))
			filter_time = np.append(filter_time, ts[i] + ((ts[i+n_samples]-ts[i])/2))
	
	return result, filter_time

#s_g1, s_ts_g1 = smooth_data(gyro1,timestamp,15)


#plt.plot(timestamp,gyro1,'-g',label='raw')
#plt.plot(s_ts_g1,s_g1,'-r',label='smoothed')
#plt.plot(hy_time,hy_pitch,'-b',label='hydrins')
#plt.legend()
#plt.show()


#Control 1: Converted GPS points into X, Y earth fixed Coordinates from initial Geodetic minutes and second in WGS84
#print(x,y)
#plt.plot(x,y,'og')
#plt.show()	
#Works, for Thesis plot one Line on Map to show equivalency to data received

##########################################################
#Now Calculate Orientation Model to prepare the last step
##########################################################

#use UKF to estimate roll pitch and yaw at every instant
#define needed rotations, prediction, measurement estimation, sigma point estimation

def rotationMatrixToEulerAngles(R):
	sy = np.sqrt(R[0,0,:]*R[0, 0, :] + R[1, 0, :] * R[1, 0, :])
	x = np.arctan2(R[2, 1, :], R[2, 2, :])
	y = np.arctan2(-R[2, 0, :], sy)
	z = np.arctan2(R[1, 0, :], R[0, 0, :])
	return x, y, z

def rotFrame(tc,ro,p,ya,t,x,y,z):
	xn = np.array([])
	yn = np.array([])
	zn = np.array([])
	idx_low = 0
	idx_high= 0
	#Find time indexes relevant for this frame 
	for i in range(len(tc)):
		if tc[i] <= np.min(t):
			idx_low = i
		if (tc[i] > np.max(t)) and (idx_high == 0):
			idx_high = i
	#print(idx_high, idx_low, tc[idx_low], np.min(t),tc[idx_high], np.max(t))

	#define rotation matrices with pitch yaw roll
	for j in np.arange(0,len(x)):
		for i in range(idx_low,idx_high):
			if (tc[i]<= t[j]) and (tc[i+1]> t[j]):
				rx = R.from_euler('x',ro[i],degrees =True)
				ry = R.from_euler('y',p[i], degrees =True)
				rz = R.from_euler('z',ya[i],degrees =True)
				r = rz*ry*rx
				new_co = r.apply([x[j],y[j],z[j]])
				xn = np.append(xn, new_co[0])
				yn = np.append(yn, new_co[1])
				zn = np.append(zn, new_co[2])

	return t, xn, yn, zn

def offsetFrame(tc,kx,ky,kvx, kvy,t,x,y, offx, offy):
	xn = np.array([])
	yn = np.array([])
	idx_low = 0
	idx_high= 0
	#Find time indexes relevant for this frame 
	for i in range(len(tc)):
		if tc[i] <= np.min(t):
			idx_low = i
		if (tc[i] > np.max(t)) and (idx_high == 0):
			idx_high = i
	#print(idx_high, idx_low, tc[idx_low], np.min(t),tc[idx_high], np.max(t))

	#define rotation matrices with pitch yaw roll
	for j in np.arange(0,len(x)):
		for i in range(idx_low,idx_high):
			if (tc[i]<= t[j]) and (tc[i+1]> t[j]):
				dt = t[j] - tc[i]
				ox = x[j] + kx[i] + (kvx[i] * dt) - offx
				oy = y[j] + ky[i] + (kvy[i] * dt) - offy
				xn = np.append(xn, ox)
				yn = np.append(yn, oy)
	return t, xn, yn

def prediction(state, control, P, Q):
	qu = vec2quat(np.atleast_2d(control).T)

	S = np.linalg.cholesky(P+Q)
	n = P.shape[0]
	S = S*np.sqrt(2*n)
	W = np.array(np.hstack([S, -S]))

	noise_quaternions = vec2quat(W)
	X = multiply(noise_quaternions.T, state)

	Y = multiply(X, qu.T)

	next_state, error = q_mean(Y.T, state)

	next_cov = np.dot(error, error.T)/12.0

	return next_state, next_cov, Y, error

def convert_measurements(acc, gyro,data_num):
	#sens_a = 330.0
	#sf_a = 3300 / 1023.0 / sens_a
	# Make sure that the first point is [0,0,1]
	bias_g =np.array([np.mean(gyro[0,:100]),np.mean(gyro[1,:100]),np.mean(gyro[2,:100])])
	
	#if data_num in [1, 2, 3, 4]:
		#bias_g = np.array([-5.798347, -7.646035, 8.052])
		#sens_g = 3.3
	#elif data_num == 5:
	#	bias_g = np.array([-5.798347, -7.646035, 8.052])
		#sens_g= 3.2
	#else:
	#	bias_g = np.array([-5.798347, -7.646035, 8.052])
		#sens_g = 3.2
	#acc_scale_factor = 3300 / 1023.0 / sens_a
	#acc_bias = np.array([-0.041111,-0.0707,-0.0435])#acc[0, :] - (np.array([0, 0, 1]) / acc_scale_factor)
	acc_bias = np.array([np.mean(acc[0,:100]),np.mean(acc[1,:100]),np.mean(acc[2,:100])])
	acc_corr = (acc - acc_bias) #* acc_scale_factor
	#gyro_scale_factor = 3300/1023/sens_g
	gyro_corr = (gyro - bias_g)#*gyro_scale_factor
	return acc_corr, gyro_corr

def estimate_rot(data_num=1):
	# Data from dataset
	#imu = read_data_imu(data_num)
	#imu_np_data = np.array(imu['vals'], dtype=np.float64).T
	imu_ts = timestamp.T

	a_x = accx #roll axis #-np.array(imu_np_data[:, 0])
	a_y = accy #pitch axis#-np.array(imu_np_data[:, 1])
	a_z = accz #yaw axis#accznp.array(imu_np_data[:, 2])
	acc = np.array([a_x, a_y, a_z]).T

	g_x = -gyro3 #np.array(imu_np_data[:, 4])
	g_y = -gyro1 #np.array(imu_np_data[:, 5])
	g_z = -gyro2 #np.array(imu_np_data[:, 3])
	gyro = np.array([g_x, g_y, g_z]).T
	acc_val, gyro_val = convert_measurements(acc, gyro,data_num)
	curr_state = np.array([1, 0, 0, 0])

		
	P =  np.identity(3)  # 0.00001
	Q = np.array([[0.5, 0, 0],
				[0, .5, 0],
				[0, 0, 0.5]])
	R = np.array([[0.64, 0, 0],
        		[0, 0.74, 0],
        		[0, 0, 0.58]])
	print(P,Q,R)
	rpy = []
	prev_timestep = imu_ts[0] - 0.01
	for i in range(1, imu_ts.shape[0]):
		control = gyro_val[i] * (imu_ts[i] - prev_timestep)
		prev_timestep = imu_ts[i]

		curr_state, P, sigma_points, error = prediction(curr_state, control, P, Q)

		Z_simga_pts = multiply(quat_inv(sigma_points), np.array([0, 0, 0, 1]))
		Z_simga_pts = multiply(Z_simga_pts, sigma_points)
		Z_simga_pts = Z_simga_pts[:, 1:]

		z_est = np.mean(Z_simga_pts, axis=0)

		Z_error = (Z_simga_pts - z_est).T
		P_zz = np.dot(Z_error, Z_error.T) / 12.0
		P_vv = P_zz + R
		P_xz = np.dot(error, Z_error.T) / 12.0
		K = np.dot(P_xz, np.linalg.inv(P_vv))
		v = np.transpose(acc_val[i] - z_est)
		Knu = vec2quat(np.atleast_2d(np.dot(K, v )).T)
		curr_state = multiply(Knu.T, curr_state).reshape(4, )
		P = P - np.dot(np.dot(K, P_vv), np.transpose(K))

		rpy.append(quat2rpy(curr_state))

	rpy = np.array(rpy)
	roll = np.array(rpy[:, 0])
	pitch = np.array(rpy[:, 1])
	yaw = np.array(rpy[:, 2])

	return roll, pitch, yaw


#if __name__ == '__main__':
	data_num=1
	t0 = time.time()
	r, p, ya = estimate_rot(data_num)
	#correct bias from acc and gyro data 
	#acrr, gycrr = convert_measurements(np.array([accx,accy,accz]).T, np.array([gyro2,gyro1,gyro3]).T, 1)
	#print(acrr, gycrr)
	#roll=np.arctan(acrr[:,1]/np.sqrt(acrr[:,0]**2+acrr[:,2]**2))
	#pitch = np.arctan(acrr[:,0]/np.sqrt(acrr[:,1]**2 + acrr[:,2]**2))
	#print(np.shape(r),np.shape(timestamp))
	#plt.figure()
	#plt.plot(np.arange(0,len(r)),r,'-r',label ='UKF Roll')
	#plt.plot(np.arange(0,len(roll)),roll,'ob',label='Acc Roll')
	#plt.xlabel('Pointnumber')
	#plt.ylabel('Roll in [Deg]')
	#plt.legend()
	#plt.show()


	##compare to hydrins
	#filename='/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/velodynehdl32e_live_data_project/testing/resultt/hydrins_slice.dat'
	#data = pd.read_csv(filename, sep=',')
	#hy_time = data.iloc[:,1]
	#hy_lat=data.iloc[:,6]
	#hy_long= data.iloc[:,7]
	#hy_pitch = data.iloc[:,8]
	#hy_roll = data.iloc[:,10]
	#umrechnen zeit in toh in mus
	#h_time_stoh=np.array([])
	#for i in np.arange(0, len(hy_time)):
	#	timestamp_obj = datetime.strptime(hy_time[i], '%Y/%m/%d %H:%M:%S.%f')
	#	h_time_stoh = np.append(h_time_stoh,(timestamp_obj.minute*60000000+timestamp_obj.second*1000000+timestamp_obj.microsecond))



	#fig, ((ax1,ax2),(ax3,ax4),(ax5,ax6)) = plt.subplots(3,2)
	#ax1.plot(timestamp, gycrr[:,0],'-r',label='Velodyne')
	#ax1.plot(timestamp[1:], r,'-b', label='UKF')
	#ax1.plot(h_time_stoh, hy_roll,'-g',label='Hydrins')
	#ax2.plot(dec_lat,dec_lon*(-1.),'-r',label='Velodyne')
	#ax2.plot(hy_lat,hy_long,'-g',label='Hydrins')
	#ax3.plot(timestamp, gycrr[:,1],'-r',label='Velodyne')
	#ax3.plot(timestamp[1:], p,'-b', label='UKF')
	#ax3.plot(h_time_stoh, hy_pitch,'-g',label='Hydrins')
	#ax4.plot(timestamp, roll, '-r', label='Velodyne')
	#ax4.plot(h_time_stoh,hy_roll,'-g', label='Hydrins')
	#ax5.plot(timestamp, gycrr[:,2],'-r',label='Velodyne')
	#ax5.plot(timestamp[1:], ya,'-b', label='UKF')
	#ax6.plot(timestamp, pitch, '-r', label='Velodyne')
	#ax6.plot(h_time_stoh,hy_pitch,'-g', label='Hydrins')
	#plt.legend()
	#plt.show()

	##comparing plots:




	#filename='/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/velodynehdl32e_live_data_project/testing/resultt/velodynehdl32e/data_ascii/8_frame_491238.955327.txt'
	#data = pd.read_csv(filename,sep=',')
	#t =data['# Time [musec]']
	#orgx =data[' X [m]']
	#orgy =data[' Y [m]']
	#orgz =data[' Z [m]']

	#print(np.sqrt(accx[0]**2+accy[0]**2+accz[0]**2))
	#tilt = np.arccos(accz[0]/np.sqrt(accx[0]**2+accy[0]**2+accz[0]**2))
	#tilt_deg = tilt*180/np.pi
	#print('tilt degree: ', tilt_deg)

	#tilt all xyz points into plane
	#rtil = R.from_euler('z',tilt_deg, degrees=True)
#	orgco =zip(orgx,orgy,orgz)
#	tilx = np.array([])
#	tily = np.array([])
#	tilz = np.array([])
	#for coo in orgco:
	#	[x,y,z]= rtil.apply(coo)
#		tilx = np.append(tilx,x)
#		tily = np.append(tily,y)
#		tilz = np.append(tilz,z)

	#ts,corrx, corry, corrz = rotFrame(timestamp[1:],r,p,ya,t,x,y,z)
	#print(co_xyz)
	#fig=plt.figure()
	#ax=fig.add_subplot(projection='3d')
	#ax.scatter(corrx,corry,corrz)
	#plt.show()



###setup: 
#NOTE THIS IS ONLY THE POSITION ESTIMATE NOT THE ORIENTATION MODEL######
#this is done in xy right now. Z is only measured by accelerometers and solved by merging the point coulds in oversampled areas
#used model and matrices:
# state matrix: x = [x,y,dx,dy]; features position and velocity
def kalman_xy(x, P, measurement, R,pre_t,t,
              Q = np.array(np.eye(4))):
	dt= t - pre_t
	pre_t = t			
	return kalman(x, P, measurement, R, Q, pre_t,
					F=np.array([[1.0, 0.0, dt, 0.0],
								[0.0, 1.0, 0.0, dt],
								[0.0, 0.0, 1.0, 0.0],
								[0.0, 0.0, 0.0, 1.0]]),
					H=np.array([[1.0, 0.0, 0.0, 0.0],
								[0.0, 1.0, 0.0, 0.0]]))

def kalman(x, P, measurement, R, Q,pre_t, F, H):

	y = np.array(measurement).T - np.dot(H,x)
	S = H.dot(P).dot(H.T) + R  # residual convariance
	K = np.dot((P.dot(H.T)), np.linalg.pinv(S))
	x = x + K.dot(y)
	I = np.array(np.eye(F.shape[0]))  # identity matrix
	P = np.dot((I - np.dot(K,H)),P)

    # PREDICT x, P
	x = np.dot(F,x)
	P = F.dot(P).dot(F.T) + Q

	return x, P, pre_t

##initial points:
#calculate orientation changes 
roll,pitch,yaw = estimate_rot()  
x=np.array([ec_x[0],ec_y[0],0.01,0.01]).T #initial state
P = np.array(np.eye(4))*1000 #initial uncertainty
pos = []
vel = []
R2=0.01**2
prev_t = timestamp[0]
for meas in zip(ec_x,ec_y,timestamp):
	x,P,pr_t = kalman_xy(x,P,meas[0:2],R2,prev_t,meas[2])
	prev_t =pr_t
	pos.append((x[:2]).tolist())
	vel.append((x[2:]).tolist())
	kalman_x, kalman_y = zip(*pos )
	kalman_vx, kalman_vy = zip(*vel)
kalman_d = pd.DataFrame(list(zip(timestamp[1:],kalman_x,kalman_y,kalman_vx,kalman_vy,roll,pitch,yaw)), columns =['Time','Kalman X', 'Kalman Y', 'Velocity Kalman X','Velocity Kalman Y','Roll','Pitch','Yaw'])
kalman_d.to_csv('/Users/LottaDraeger/Desktop/kalman_track.txt')
#print(kalman_x,kalman_y)
#h_ec_x=np.array([])
#h_ec_y=np.array([])

#for i in np.arange(0,len(hy_long)):
#	xp,yp = trans.transform(hy_long[i],hy_lat[i])
#	h_ec_x=np.append(h_ec_x,xp)
#	h_ec_y=np.append(h_ec_y,yp)

plt.plot(ec_x,ec_y,'ro')
plt.plot(kalman_x,kalman_y,'go')
plt.show()


#for one single frame here;
#correct frames with positional offeset into one data frame
#ts,rotx, roty, rotz = rotFrame(timestamp[1:],r,p,ya,t,tilx,tily,tilz)
#rotation of the frame to have z as hight parameter
#hix = rotz
#hiy = roty
#hiz = -rotx

#print(np.shape(timestamp[1:]),np.shape(r),np.shape(p),np.shape(ya),np.shape(t))
#print(np.shape(kalman_x),np.shape(kalman_y),np.shape(kalman_vx),np.shape(kalman_vy),np.shape(ts),np.shape(corrx),np.shape(corry))
#print(np.shape(ts),np.shape(rotx))
#ti, finalx, finaly = offsetFrame(timestamp[1:],kalman_x,kalman_y,kalman_vx, kalman_vy,ts,hix,hiy, orgz[0], orgy[0])
#fig=plt.figure()
#ax=fig.add_subplot(projection='3d')
#ax.scatter(finalx,finaly,hiz)
#plt.show()

#for the first 10 txt files to plot together
#directory ='/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/GIT Abgabe/velodynehdl32e_live_dem/testing/lrtest/velodynehdl32e/data_ascii'
directory = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/data_ascii/pic_1120'
#intialise plot arrays

#iterate over txt results file in this directory 
os.chdir(directory)
for filename in os.listdir():
	if filename.endswith('.txt'):
		print('Reading file: ', filename)

		#reading in the data into pandas data frame
		data =pd.read_csv(filename, sep=',')

		t_mul =data['# Time [musec]']
		x =data[' X [m]'].to_numpy()
		y =data[' Y [m]'].to_numpy()
		z =data[' Z [m]'].to_numpy()
		intensity = data[' Intensity'].to_numpy()


		plot_x.append(x)
		plot_y.append(y)
		plot_z.append(z)

ti, finalx, finaly = offsetFrame(timestamp[1:],kalman_x,kalman_y,kalman_vx, kalman_vy,t_mul,plot_x,plot_z, plot_z[0], plot_y[0])



fig, ((ax1),(ax2),(ax3)) = plt.subplots(1,3)

ax1.plot(finalx,finaly,'ob',label='X-Y Ebene')
ax2.plot(finalx,plot_z,'ob',label='X-Z Ebene')
ax3.plot(finaly,plot_z,'ob',label='Y-Z Ebene')
plt.legend()
plt.show()


#fig=plt.figure()
#ax=fig.add_subplot(projection='3d')
#ax.scatter(plot_x,plot_y,plot_z)
#plt.show()


