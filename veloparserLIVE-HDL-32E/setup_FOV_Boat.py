import pycurl 
try:
	from io import BytesIO 
except ImportError:
	from StringIO import StringIO as BytesIO 
try:
	from urllib.parse import urlencode 
except ImportError:
	from urllib import urlencode 

import urllib.request as urllib2
import json 
import time

def sensor_do(s, url, pf, buf): 
	s.setopt(s.URL, url) 
	s.setopt(s.POSTFIELDS, pf) 
	s.setopt(s.WRITEDATA, buf) 
	s.perform()
	rcode = s.getinfo(s.RESPONSE_CODE) 
	success = rcode in range(200, 207) 
	print('%s %s: %d (%s)' % (url, pf,rcode, 'OK' if success else 'ERROR'))
	return success


#insert correct IP Address here in the BASE URL to address the sensor 
Base_URL = 'http://192.168.1.201/cgi/'

sensor = pycurl.Curl() 
buffer = BytesIO()

#set rpm to 300 
sensor_do(sensor, Base_URL+'setting', urlencode({'rpm':'300'}), buffer)
#set fov do determined end and beginning FOV
sensor_do(sensor,Base_URL+'setting/fov', urlencode({'start':'0'}), buffer)
sensor_do(sensor,Base_URL+'setting/fov', urlencode({'end':'120'}), buffer)
#set return type (last or strongest)
sensor_do(sensor, Base_URL+'setting', urlencode({'returns':'strongest'}), buffer)

#get status of sensor 
response = urllib2.urlopen(Base_URL+'status.json') 
if response:
	status = json.loads(response.read())
#print 'FOV is set from %s to %s, motor rpm is %s , return mode is set to %s' % \
#(status['laser']['state'], status['motor']['rpm'])
print(status)
sensor.close()


