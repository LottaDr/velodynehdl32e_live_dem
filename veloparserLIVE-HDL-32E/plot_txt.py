#script to calculate point cloud files from txt files without any other correction / transformation

import matplotlib.pyplot as plt
from matplotlib import cm 
import matplotlib as mpl
import numpy as np
import pandas as pd
import os
import open3d as o3d
#directory ='/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/GIT Abgabe/velodynehdl32e_live_dem/testing/offline/lrtest/velodynehdl32e/data_ascii'
#directory = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-0832/velodyne32hdl/data_ascii/pic_9300'
directory = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/data_ascii/pic_1120'
#intialise plot arrays
plot_x=np.array([])
plot_y=np.array([])
plot_z=np.array([])
colr = np.array([])
colg = np.array([])
colb = np.array([])
#intensities =np.array([])

#define colour scale
norm = mpl.colors.Normalize(vmin=0, vmax=255)
#iterate over txt results file in this directory 
os.chdir(directory)
i=0
for filename in os.listdir():
	if filename.endswith('.txt'):
		print('Reading file: ', filename)

		#reading in the data into pandas data frame
		data =pd.read_csv(filename, sep=',')

		t_mul =data['# Time [musec]']
		x =data[' X [m]'].to_numpy()
		y =data[' Y [m]'].to_numpy()
		z =data[' Z [m]'].to_numpy()
		intensity = data[' Intensity'].to_numpy()
		if intensity is not None:
			intensities = norm(intensity)
			col = cm.plasma(intensities)

		plot_x = np.append(plot_x, x)
		plot_y = np.append(plot_y, y)
		plot_z = np.append(plot_z, z)
		colr = np.append(colr, col[:,0])
		colg = np.append(colg, col[:,1])
		colb = np.append(colb, col[:,2])

	#device = o3d.core.Device("CPU:0")
	#dtype = o3d.core.float32

	#format points into point cloud object
	pcd= o3d.geometry.PointCloud()
	xyz= np.zeros((len(plot_x),3))
	xyz[:,0]=np.reshape(plot_y,-1)
	xyz[:,1]=np.reshape(plot_z,-1)
	xyz[:,2]=np.reshape(plot_x,-1)

	rgb = np.zeros((len(colr),3))
	rgb[:,0]=np.reshape(colr,-1)
	rgb[:,1]=np.reshape(colb,-1)
	rgb[:,2]=np.reshape(colg,-1)
	#inte =np.zeros((len(intensities),1))
	#inte[:,0]=np.reshape(intensities,-1)
		#print(xyz)
	#pcd.point = o3d.utility.Vector3dVector(xyz)
	pcd.points = o3d.utility.Vector3dVector(xyz)
	pcd.colors = o3d.utility.Vector3dVector(rgb)

	#write point cloud
	o3d.io.write_point_cloud("pcds/nocor/pcl_frame_{}.pcd".format(i), pcd)
	#o3d.visualization.draw(pcd)
	i+=1


fig=plt.figure()
ax=fig.add_subplot(projection='3d')
ax.scatter(plot_x,plot_y,plot_z)


ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()