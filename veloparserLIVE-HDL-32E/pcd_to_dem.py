import open3d as o3d
import os 
import numpy as np
#import copy
import math
import itertools
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R
import pandas as pd
import matplotlib as mpl
from matplotlib import cm


#rotation of the plane given normal vector
def rotate_plane(a,b,c,d, pcd):
	n = np.array([a,b,c])*np.sqrt(a*a+b*b+c*c)
	n = -d/np.sqrt(a*a+b*b+c*c)*n
	#now rotate to system that is in xy plane
	nxny = np.sqrt(n[0]*n[0]+n[1]*n[1])
	r = np.array([[n[1]/nxny, -n[0]/nxny,0],[n[0]*n[2]/nxny,n[1]*n[2]/nxny,-nxny],[n[0],n[1],n[2]]])

	rot =R.from_matrix(r)
	pcd_rot = rot.apply(pcd)

	return pcd_rot

#reduction of the plane by adjusting z coordinate
def red_plane(a,b,c,d,points):
	z_red = points[:,2] - (((-d-a*points[:,0])-(a*points[:,1]))/ c )
	#print(a,b,c,d,z_red)
	results = np.zeros((len(z_red),3))
	results[:,0]=np.reshape(points[:,0],-1)
	results[:,1]=np.reshape(points[:,1],-1)
	results[:,2]=np.reshape(z_red,-1)
	return results

#load point clouds and correct plane orientation
def load_point_clouds(voxel_size=0.0):

	directory = '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/data_ascii/pic_1120/pcds'
	geometry = o3d.geometry.PointCloud()
	#off_pcd = o3d.geometry.PointCloud()
	inlier_cloud =o3d.geometry.PointCloud()
	outlier_cloud = o3d.geometry.PointCloud()
	#agree=0.0
	#i=0
	os.chdir(directory)
	for filename in os.listdir():
		if filename.endswith('.pcd'):
			print(filename)
			pcd = o3d.io.read_point_cloud(filename)
			points_pcd = np.asarray(pcd.points)
			#plt.scatter(points[:,1],points[:,0],s=0.2,c='b')
			#plt.xlabel('ECEF Y Coordinate in [km]')
			#plt.ylabel('ECEF X Coordinate in [km]')
			#plt.show()
			#plt.savefig('frame_{}.png'.format(i))
			#plt.clf()

			#plane detection
			plane_model, inliers = pcd.segment_plane(distance_threshold = 0.1, ransac_n = 3, num_iterations = 10000)
			[a,b,c,d] = plane_model
			inlier_cloud = pcd.select_by_index(inliers)
			outlier_cloud = pcd.select_by_index(inliers, invert=True)
			inlier_cloud.paint_uniform_color([1.0, 0, 0])
			#o3d.visualization.draw([inlier_cloud, outlier_cloud])

			#reduce plane orientation
			red_points = red_plane(a,b,c,d,points)
			geometry.points.extend(o3d.utility.Vector3dVector(red_points))
			points = np.asarray(geometry.points)
			geometry.points = o3d.utility.Vector3dVector(points)
			#red_pcd.points.extend(o3d.utility.Vector3dVector(red_points))
			#o3d.visualization.draw(geometry)
	print("All PCDs loaded")
	return geometry #, red_pcd
#function to display inlier and outlier clouds
def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_by_index(ind)
    outlier_cloud = cloud.select_by_index(ind, invert=True)

    print("Showing outliers (red) and inliers (gray): ")
    outlier_cloud.paint_uniform_color([1, 0, 0])
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


def create_gdem(points, grid_size ):
	x = points[:,0]
	y = points[:,1]
	z = points[:,2]

	x_min = np.min(x)
	y_min = np.min(y)
	x_max = np.max(x)
	y_max = np.max(y)


	#create grid in x,y direction and sort measurements into squares to compute mean height
	xaxis = np.arange(x_min,x_max,grid_size)
	yaxis = np.arange(y_min,y_max,grid_size)
	xgrid, ygrid = np.meshgrid(xaxis,yaxis)

	#determine the height of each grid segment
	xn = (x-x_min)/grid_size
	yn = (y-y_min)/grid_size

	xn = xn.astype(int)
	yn = yn.astype(int)

	xn_max = np.max(xn)
	yn_max = np.max(yn)

	print(xn_max,yn_max)

	dfgrid =pd.DataFrame({'x': xn, 'y':yn, 'z':z},columns=['x','y','z'])

	count = dfgrid.groupby(['x','y'], as_index =False)['z'].size().apply(np.array)
	mean_std = dfgrid.groupby(['x','y'], as_index =False)['z'].agg([np.mean,np.std,]).apply(np.array)
	print(np.shape(count), np.shape(mean_std))
	#sort = pd.DataFrame({'x': count.iloc[:,0], 'y': count.iloc[:,1], 'mean': mean_std.iloc[:,0], 'std': mean_std.iloc[:,1],'size': count.iloc[:,2]}, columns =['x','y','mean','std','size'])
	sort = np.hstack([count,mean_std])
	print(sort)

	return sort


if __name__ == "__main__":
	voxel_size=0.02
	pcd = load_point_clouds(voxel_size)

	pcds = np.asarray(pcd.points)

	plt.scatter(pcds[:,0],-pcds[:,1], s=0.2, c='b')
	plt.show()

	plt.scatter(pcds[:,1],pcds[:,2], s=0.2, c='b')
	plt.show()

	#print(pcd_rot)

	o3d.visualization.draw(pcd)
	#o3d.visualization.draw(red_pcd)

	#pcdr= o3d.geometry.PointCloud()
	#pcdr.points = o3d.utility.Vector3dVector(pcd_rot)

	#o3d.visualization.draw(pcdr)



	print("Crop Point Cloud")
	bounds = [[-math.inf, math.inf], [-math.inf, math.inf], [-20, -5]]  # set the bounds
	bounding_box_points = list(itertools.product(*bounds))  # create limit points
	bounding_box = o3d.geometry.AxisAlignedBoundingBox.create_from_points(
		o3d.utility.Vector3dVector(bounding_box_points))  # create bounding box object
	pcd_cropped = pcd.crop(bounding_box)
	#o3d.visualization.draw(pcd_cropped)

	pcd_cr =np.asarray(pcd_cropped.points)

	plt.scatter(pcd_cr[:,0],-pcd_cr[:,1], s=0.2, c='b')
	plt.show()

	plane_model, inliers = pcd.segment_plane(distance_threshold = 0.01, ransac_n = 3, num_iterations = 1000)
	[a,b,c,d] = plane_model
	print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

	inlier_cloud = pcd.select_by_index(inliers)
	inlier_cloud.paint_uniform_color([1.0, 0, 0])
	outlier_cloud = pcd.select_by_index(inliers, invert=True)
	o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud], zoom=0.8,
                                  front=[-0.4999, -0.1659, -0.8499],
                                  lookat=[2.1813, 2.0619, 2.0999],
                                  up=[0.1204, -0.9852, 0.1215])
	pcd_in = np.asarray(inlier_cloud.points)
	pcd_out = np.asarray(outlier_cloud.points)

	plt.scatter(pcd_in[:,0],pcd_in[:,1], s=0.2, c='r')
	plt.scatter(pcd_out[:,0],pcd_out[:,1], s=0.2, c='b')
	plt.show()

	print("Statistical outlier removal")
	cl, ind = pcd_cropped.remove_statistical_outlier(nb_neighbors=20,
                                                    std_ratio=2.0)
	display_inlier_outlier(pcd_cropped, ind)
	pcd_filtered = pcd_cropped.select_by_index(ind)
	pcd_filtered.estimate_normals()

	pcd_filtered = pcd_filtered.voxel_down_sample(voxel_size=0.1)

	voxel_size = 1.0 

	points = np.asarray(pcd_cropped.points)

	gdem = create_gdem(points, voxel_size)
	print(gdem)

	gdem_fil = gdem[np.where(gdem[:,3]<=-14.5)]

	norm = mpl.colors.Normalize(vmin=np.min(gdem_fil[:,2]), vmax=np.max(gdem_fil[:,2]))
	gdem_col = norm(gdem_fil[:,2])
	col = cm.plasma(gdem_col)

	fig = plt.figure()
	ax = fig.add_subplot(projection='3d')

	ax.scatter(gdem_fil[:,0],gdem_fil[:,1],gdem_fil[:,3], s=0.2, c='b')
	plt.show()

	fig=plt.figure()
	ax1 = fig.add_subplot(projection='3d')
	ax1.bar3d(gdem_fil[:,0], gdem_fil[:,1], np.zeros(len(gdem_fil[:,0])), voxel_size, voxel_size, (-gdem_fil[:,3]), shade=True, color =col)
	ax1.set_xlabel('Square Number along ECEF X axis')
	ax1.set_ylabel('Square Number along ECEF Y axis')
	ax1.set_zlabel('Height in [m]')
	plt.show()

