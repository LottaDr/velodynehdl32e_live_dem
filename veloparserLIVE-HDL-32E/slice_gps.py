import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
def gms_to_gdec(gms):
	g =int(gms/100)
	m =gms -g*100
	gdec = g+m/60
	#print(gdec)
	return gdec

filename1= '/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/velodynehdl32e_live_data_project/testing/resultt/hydrins_slice.dat'
data1 = pd.read_csv(filename1, sep=',')

filename2= '/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/velodynehdl32e_live_data_project/testing/resultt/gps_slice.dat'
data2 = pd.read_csv(filename2, sep=',')

v_lat= data2[' Latitude [Deg]'].to_numpy()
v_long = data2[' Longitudes [Deg]'].to_numpy()
h_lat = data1.iloc[:,6]
h_long = data1.iloc[:,7]

v_lat_dec=np.zeros(len(v_lat))
v_long_dec=np.zeros(len(v_long))

for x in np.arange(0,len(v_lat)):	
	v_lat_dec[x] =gms_to_gdec(v_lat[x])
	v_long_dec[x] = gms_to_gdec(v_long[x])*(-1.)

#v_lat_dec=v_lat/100
#v_long_dec=v_long/100*(-1.)


print(v_lat_dec, h_lat)
print(v_long_dec,h_long)
#fig, ((ax1,ax2)) =plt.subplots(2,1)
plt.plot(v_lat_dec,v_long_dec,'ob',label='velodyne')
plt.plot(h_lat,h_long,'or',label='hydrins')
plt.legend()
plt.show()



#data.iloc[:,6]=data.iloc[:,6]/100
#data.iloc[:,8]=data.iloc[:,8]/100
#for x in np.arange(0,len(data.iloc[:,9])):
#	if data.iloc[x,9] == ' W':
#		data.iloc[x,8] = data.iloc[x,8]*(-1.)
#		data.iloc[x,9] = 'E'
#print(data.iloc[:,8],data.iloc[:,9])
#data.to_csv('/Users/LottaDraeger/Desktop/Leben/Studium/Uni_Bremen/Master_Project/programs/velodynehdl32e_live_data_project/testing/resultt/gps_corr_slice.dat')
