import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pyproj import Transformer
from pyproj import CRS
import os
import matplotlib as mpl
from matplotlib import cm
import open3d as o3d
from scipy.spatial.transform import Rotation as R

#setup Mesurements first
#GPS from Lat Long in WGS4 to XYZ Earth Bound
#define wgs84 as CRS EPSG:4326
wgs84 = CRS.from_epsg(4326)
#define earth centred cartesian coordinates as CRS EPSG:4978
ccwgs = CRS.from_epsg(4978)

#define transformation
trans = Transformer.from_crs(4326,4978)
norm = mpl.colors.Normalize(vmin=0, vmax=255)

#import kalman calulations done before
filename= '/Users/LottaDraeger/Desktop/kalman_track.txt'
data = pd.read_csv(filename,sep=',')
time_kal = data['Time'].to_numpy()
kalman_x = data['Kalman X'].to_numpy()
kalman_y = data['Kalman Y'].to_numpy()
kalman_vx = data['Velocity Kalman X'].to_numpy()
kalman_vy = data['Velocity Kalman Y'].to_numpy()
kalman_pi = data['Pitch'].to_numpy()
kalman_ya = data['Yaw'].to_numpy()
kalman_ro = data['Roll'].to_numpy()
velocity_kal = np.sqrt(kalman_vx**2+kalman_vy**2)
velocity_kal *= (1/velocity_kal.max())

def gms_to_gdec(gms):
	g =int(gms/100)
	m =gms -g*100
	gdec = g+m/60
	#print(gdec)
	return gdec

#input GPS data from file
lat=np.array([]) #4 
lon=np.array([]) #5

#filename of the gps measurements for comparison
filename= '/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/gps_corr_slice.dat'
data = pd.read_csv(filename,sep=',')
data = data.iloc[:4247]
#latitude longitude Data
lat=data[' Latitude [Deg]'].to_numpy()
lon= data[' Longitudes [Deg]'].to_numpy()
vel = data[' Velocity [m/s]'].to_numpy()
vel_gps = vel*(1/vel.max())

#convert to degree
dec_lat = np.array([])
dec_lon = np.array([])
for i in np.arange(0,len(lat)):
	#print(i)
	dec_lat = np.append(dec_lat,gms_to_gdec(lat[i]))
	dec_lon = np.append(dec_lon,gms_to_gdec(lon[i]))
#print(dec_lat,dec_lon)
#compute heading
A = np.cos(dec_lon[30])*np.sin(dec_lat[30]-dec_lat[0])
B = np.cos(dec_lon[0])*np.sin(dec_lon[30])- np.sin(dec_lon[0])*np.cos(dec_lon[30])*np.cos(dec_lat[30]-dec_lat[0])
head =np.arctan2(A,B) 
print(head)
#convert from wgs 84 to earthcentred cartesian
ec_x=np.array([])
ec_y=np.array([])

for i in np.arange(0,len(dec_lon)):
	xp,yp = trans.transform(dec_lon[i],dec_lat[i])
	ec_x=np.append(ec_x,xp)
	ec_y=np.append(ec_y,yp)

#print(ec_y,ec_x)

#show they imported correctly
plt.plot(kalman_y,kalman_x,'ob', label='Kalman Fit')
plt.plot(ec_y,ec_x,'-g', label='Velodyne GPS')
#plt.colorbar(label='Velocity')
plt.legend()
plt.show()

#define the offset frame function
def offsetFrame(tc,kx,ky,kvx, kvy,t,x,y, offx, offy):
	xn = np.array([])
	yn = np.array([])
	idx_low = 0
	idx_high= 0
	#Find time indexes relevant for this frame 
	for i in range(len(tc)):
		if tc[i] <= np.min(t):
			idx_low = i
		if (tc[i] > np.max(t)) and (idx_high == 0):
			idx_high = i
	#print(idx_high, idx_low, tc[idx_low], np.min(t),tc[idx_high], np.max(t))

	#calc offset based on time
	for j in np.arange(0,len(x)):
		for i in range(idx_low,idx_high):
			#print(i)
			if (tc[i]<= t[j]) and (tc[i+1]> t[j]):
				dt = t[j] - tc[i]
				ox = x[j] + kx[i] + (kvx[i] * dt) #- offx
				#ox = x[j]
				oy = y[j] + ky[i] + (kvy[i] * dt) #- offy
				xn = np.append(xn, ox)
				yn = np.append(yn, oy)
	return t, xn, yn

#define the roll, pitch,yaw rotation of the point cloud
def rotFrame(tc,ro,p,ya,t,x,y,z):
	xn = np.array([])
	yn = np.array([])
	zn = np.array([])
	idx_low = 0
	idx_high= 0
	#Find time indexes relevant for this frame 
	for i in range(len(tc)):
		if tc[i] <= np.min(t):
			idx_low = i
		if (tc[i] > np.max(t)) and (idx_high == 0):
			idx_high = i
	#print(idx_high, idx_low, tc[idx_low], np.min(t),tc[idx_high], np.max(t))

	#define rotation matrices with pitch yaw roll
	for j in np.arange(0,len(x)):
		for i in range(idx_low,idx_high):
			if (tc[i]<= t[j]) and (tc[i+1]> t[j]):
				rx = R.from_euler('x',ro[i],degrees =True)
				ry = R.from_euler('y',p[i], degrees =True)
				rz = R.from_euler('z',ya[i],degrees =True)
				r = rz*ry*rx
				new_co = r.apply([x[j],y[j],z[j]])
				xn = np.append(xn, new_co[0])
				yn = np.append(yn, new_co[1])
				zn = np.append(zn, new_co[2])

	return t, xn, yn, zn




#location of the txt files with data points
directory ='/Volumes/LOTTA/Master/Archiv/velodyne-recordings-crane-20220729-1018/velodyne32hdl/data_ascii/pic_1120'
#intialise plot arrays
plot_x= []
plot_y= []
plot_z= []
p_r = np.array([])
p_g = np.array([])
p_b = np.array([])

#iterate over txt results file in this directory 
os.chdir(directory)
i=0
for filename in os.listdir():
	if filename.endswith('.txt'):
		print('Reading file: ', filename)

		#reading in the data into pandas data frame
		data =pd.read_csv(filename, sep=',')

		#read in data
		t_mul =data['# Time [musec]']
		orgx_mul =data[' X [m]']
		orgy_mul =data[' Y [m]']
		orgz_mul =data[' Z [m]']
		intensity = data[' Intensity'].to_numpy()

		#calculate rgb value of each point given by reflected intensity
		intensity = norm(intensity)
		col = cm.plasma(intensity)
		x = orgz_mul
		y = orgy_mul
		z = orgx_mul

		#fig, ((ax1),(ax2),(ax3)) = plt.subplots(1,3)

		#ax1.plot(x,y,'ob',label='X-Y Ebene')
		#ax1.set_xlabel('x')
		#ax1.set_ylabel('y')
		#print(kalman_y)
		#ax2.plot(kalman_x,kalman_y,'og')

		#ax3.plot(y,z,'ob',label='Y-Z Ebene')
		#plt.show()

		#plt.scatter(y,x)
		#plt.show()

		#correct orientation and position changes
		#t_mul, x,y,z = rotFrame(time_kal,kalman_ro,kalman_pi,kalman_ya,t_mul,x,y,z)
		#ti_mul, x, y = offsetFrame(time_kal,kalman_y,kalman_x,kalman_vy, kalman_vx,t_mul,y,x, y[0], x[0])
		print(x,y,z)

		#r = R.from_euler('z',head,degrees =True)
		#for i in range(0,len(x)):
		#	[x[i],y[i],z[i]] = r.apply([x[i],y[i],z[i]])

		plot_x=np.append(plot_x,x)
		plot_y=np.append(plot_y,y)
		plot_z=np.append(plot_z,z)

		#format results into point cloud object
		pcd= o3d.geometry.PointCloud()
		xyz= np.zeros((len(x),3))
		xyz[:,0]=np.reshape(x,-1)
		xyz[:,1]=np.reshape(y,-1)
		xyz[:,2]=np.reshape(z,-1)
		p_r = np.append(p_r, col[:,0])
		p_g = np.append(p_g, col[:,1])
		p_b = np.append(p_b, col[:,2])
		rgb = np.zeros((len(p_r),3))
		rgb[:,0]=np.reshape(p_r,-1)
		rgb[:,1]=np.reshape(p_g,-1)
		rgb[:,2]=np.reshape(p_b,-1)

		#write output pointcloud
		pcd.points = o3d.utility.Vector3dVector(xyz)
		pcd.colors = o3d.utility.Vector3dVector(rgb)
		print(pcd)
		#specify folder
		o3d.io.write_point_cloud("pcds/nocor/pcl_{}_frame.pcd".format(i), pcd, write_ascii=True)
		i+=1


		