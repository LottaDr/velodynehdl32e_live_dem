import os
from pathlib import Path
import dpkt
import open3d as o3d
from matplotlib import cm 
import matplotlib as mpl
import datetime
import pandas as pd
import numpy as np
from tqdm import tqdm
import pyshark
from scapy.all import *

from gps import GprmcMessage, utc_to_weekseconds
import lidar

pd.options.mode.chained_assignment = None 

#class of the lidar manager to implement the live data read out
class VelodyneManager():
    #definition of class parameters needed in data processing from pcacp file and live link
    def __init__(self, type, pcap_path, out_root, params):
        if pcap_path is not None:
            self.pcap_path = Path(pcap_path)
        self.params = params
        self.lidar_type = type
        self.lidar = None
        self.out_root = out_root

        self.pos_X = None
        self.pos_Y = None
        self.pos_Z = None
        self.intensities = None
        self.latitudes = None
        self.timestamps = None
        self.distances = None
        self.indicies = None
        self.longitudes = None

        self.frame_nr = 0
        self.cur_azimuth = None
        self.last_azimuth = None
        self.date = None
        #self.datetime=None

        self.gps_fp = None
        self.kalman_fp = None
        self.rough_fp= None

        self.lidar = lidar.VelodyneHDL32E()
        
    #for pcap read out progress bar
    def get_pcap_length(self):
        # open pcap file
        try:
            fpcap = open(self.pcap_path, 'rb')
            lidar_reader = dpkt.pcap.Reader(fpcap)
        except Exception as ex:
            print(str(ex))
            return 0

        counter = 0
        # itearte through each data packet and timestamps
        for _, _ in enumerate(lidar_reader):
            counter += 1
        fpcap.close()
        return counter

    #function to compute surface mean hight, track length and rms roughness
    def calc_r_para(self,track):
        fil_mean =np.mean(track.iloc[:,4])
        fil_std = np.std(track.iloc[:,4])
        #print(fil_mean,fil_std, track[" Z [m]"])
        fil_track = track[(track.iloc[:,4]>= (fil_mean-fil_std)) & (track.iloc[:,4] <= (fil_mean+fil_std))]
        #set filtered track as track
        track = fil_track
        #calculate track length in 3D
        track_l = np.sqrt((track.iloc[-1,2]-track.iloc[0,2])**2+(track.iloc[-1,3]-track.iloc[0,3])**2+(track.iloc[-1,4]-track.iloc[0,4])**2)
        #detrend the data using least square approach in 3D 
        fit =track.to_numpy()
        #print(fit)
        A = np.c_[fit[:,2],fit[:,3],np.ones(fit.shape[0])]
        C,e,_,_ =np.linalg.lstsq(A,fit[:,4],rcond=None)
        #z=C[0]*x + C[1]*y + C[2]
        #subtract trend from each data point
        track.iloc[:,4]  = track.iloc[:,4] - (C[0]*track.iloc[:,2] + C[1]*track.iloc[:,3] + C[2])
        
        #calculate mean hight
        hight= track.iloc[:,4]
        h_mean = hight.sum()/ track.shape[0]
        #calculate rms 
        h_dev = (hight -h_mean)**2
        h_rms = np.sqrt(h_dev.sum() / track.shape[0])
        return track_l, h_mean,h_rms
    #function to implement the live link read out
    def run_stream(self):
        """
        Extracts point clouds from pcap file
        :return:
        """
        #initialze Measurement arrays that keep data of one roll over/frame
        erg_x=np.array([])
        erg_y=np.array([])
        erg_z=np.array([])
        erg_dist =np.array([])
        erg_intensities=np.array([])
        erg_ID=np.array([])
        erg_timestamps=np.array([])
        erg_azimuth=np.array([])


         # create output folder hierarchy
        if not self.create_folders():
            return

        #sniff packets continuously
        i=0
        flag_init = True
        pcap_reader = pyshark.LiveCapture('en3',bpf_filter='udp port 2368 or udp port 8308') #sniffs all packets at ethernet 3 port with UDP format; to be adjusted when running on different system
        for pkt in pcap_reader.sniff_continuously():
            if pkt:
                #decode packet to uint32 for processing
                #print(pkt.DATA._all_fields)
                dat=hex_bytes(pkt.DATA.data)
                if self.date is None:
                    self.datetime = datetime.utcfromtimestamp(float(pkt.sniff_timestamp))
                #check if data frame or gps frame and process each frame:
                i+=1
                if len(pkt.DATA.data)== 2412:
                    self.process_data_frame(dat, pkt.sniff_timestamp,i, flag_init)
                if len(pkt.DATA.data)== 1024:
                    self.process_gps_frame(dat, pkt.sniff_timestamp, i,flag_init)
                    flag_init = False
            else: 
                print('No Packet sniffed move to next.')
                
            #updatedata 
            #self.collect_plot_data(erg_azimuth, erg_timestamps, erg_dist, timest, dist)
            #print(timest,dist)
            #make plot

            #line1 =live_plotter(erg_timestamps,erg_dist,line1)

            #pbar.update(1)

        #if self.gps_fp is not None:
         #   self.gps_fp.close()
    #function to implement the pcap read out
    def run(self):
        """
        Exteractis point clouds from pcap file
        :return:
        """

        pcap_len = self.get_pcap_length()
        if pcap_len <= 0:
            return

        # open pcap file
        try:
            fpcap = open(self.pcap_path, 'rb')
            self.lidar_reader = dpkt.pcap.Reader(fpcap)
        except Exception as ex:
            print(str(ex))
            return

        # create output folder hierarchy
        if not self.create_folders():
            return

        # itearte through each data packet and timestamps
        pbar = tqdm(total=pcap_len)
        flag_init = True
        for idx, (ts, buf) in enumerate(self.lidar_reader):

            if idx < self.params['from']:
                continue
            if 0 < self.params['to'] < idx:
                break

            if self.date is None:
                self.datetime = datetime.utcfromtimestamp(ts)

            eth = dpkt.ethernet.Ethernet(buf)
            data = eth.data.data.data

            # handle Position-Frame (GPS-Data)
            if self.params['gps']:
                if eth.data.data.sport == self.params['gps-port']:
                    self.process_gps_frame(data, ts, idx,flag_init)
                    flag_init = False
                    #print('GPS Frame')
                    

            # Handle Data-Frame (Point clouds)
            if eth.data.data.sport == self.params['data-port']:
                self.process_data_frame(data, ts, idx,flag_init)
                #print('Data Frame')

            pbar.update(1)

        if self.gps_fp is not None:
            self.gps_fp.close()
    #routine of data frame processing
    def process_data_frame(self, data, timestamp, index,flag_init):
        cur_X, cur_Y, cur_Z, cur_intensities, cur_latitudes, cur_timestamps, cur_distances = self.lidar.process_data_frame(self.params, data, index,flag_init)

        # number of sequences
        n_seq = int(len(cur_X) / self.lidar.count_lasers)

        cur_indicies = np.tile(np.arange(self.lidar.count_lasers), n_seq)
        cur_longitudes = np.tile(self.lidar.omega, n_seq)

        #apply filter to the captured packets
        if self.params['c_beam']:
        #initalize filtered arrays
            fil_longitudes=np.empty(1)
            fil_pos_X=np.empty(1)
            fil_pos_Y=np.empty(1)
            fil_pos_Z=np.empty(1)
            fil_intensities=np.empty(1)
            fil_latitudes=np.empty(1)
            fil_timestamps=np.empty(1)
            fil_distances=np.empty(1)
            fil_indicies=np.empty(1)

            print(len(cur_longitudes))
            for k in range(1,len(cur_X),1):
                if int(cur_longitudes[k]) == 0:
                    print(k)
                    fil_longitudes =np.append(fil_longitudes,cur_longitudes[k])
                    fil_pos_X =np.append(fil_pos_X,cur_X[k])
                    fil_pos_Y =np.append(fil_pos_Y,cur_Y[k])
                    fil_pos_Z =np.append(fil_pos_Z,cur_Z[k])
                    fil_intensities =np.append(fil_intensities,cur_intensities[k])
                    fil_latitudes =np.append(fil_latitudes,cur_latitudes[k])
                    fil_timestamps =np.append(fil_timestamps,cur_timestamps[k])
                    fil_distances =np.append(fil_distances,cur_distances[k])
                    fil_indicies =np.append(fil_indicies,cur_indicies[k])
            print(fil_indicies)   
            print(len(fil_indicies))       

            # initilaise states
            if index == 0 or self.pos_X is None:
                self.pos_X = fil_pos_X
                self.pos_Y = fil_pos_Y
                self.pos_Z = fil_pos_Z
                self.intensities = fil_distances
                self.latitudes = fil_latitudes
                self.timestamps = fil_timestamps
                self.distances = fil_distances
                self.indicies = fil_indicies
                self.longitudes = fil_longitudes


            if self.cur_azimuth is None:
                self.cur_azimuth = fil_latitudes
                self.last_azimuth = fil_latitudes

            # update current azimuth before checking for roll over
            self.cur_azimuth = fil_latitudes

            # check if a frame is finished
            idx_rollovr = self.is_roll_over()

            # handle rollover (full 360 degree frame/full FOV)
            if idx_rollovr is not None:
                if idx_rollovr > 0:
                    self.pos_X = np.hstack((self.pos_X, fil_pos_X[0:idx_rollovr+1]))
                    self.pos_Y = np.hstack((self.pos_Y, fil_pos_Y[0:idx_rollovr+1]))
                    self.pos_Z = np.hstack((self.pos_Z, fil_pos_Z[0:idx_rollovr+1]))
                    self.intensities = np.hstack((self.intensities, fil_intensities[0:idx_rollovr+1]))
                    self.latitudes = np.hstack((self.latitudes, fil_latitudes[0:idx_rollovr+1]))
                    self.timestamps = np.hstack((self.timestamps, fil_timestamps[0:idx_rollovr+1]))
                    self.distances = np.hstack((self.distances, fil_distances[0:idx_rollovr+1]))
                    self.indicies = np.hstack((self.indicies, fil_indicies[0:idx_rollovr+1]))
                    self.longitudes = np.hstack((self.longitudes, fil_longitudes[0:idx_rollovr+1]))

                min, sec, micro = self.time_from_lidar(self.timestamps[0])
                self.datetime = self.datetime.replace(minute=min, second=sec, microsecond=int(micro))
                gpsweek, gpsdays, gpsseconds, gpsmicrosec = utc_to_weekseconds(self.datetime, 0)

                #fill measuremtn arrays with current frame
                erg_x=fil_pos_X
                erg_y=fil_pos_Y
                erg_z=fil_pos_Z
                erg_azimuth = fil_latitudes
                erg_intensities=fil_intensities
                erg_ID=fil_indicies
                erg_timestamps =fil_timestamps

                #write current frame into txt and pcd format depending on args
                if self.params['text']:
                    fpath = "{}/{}_frame_{}.{:06d}.txt".format(self.txt_path, self.frame_nr, gpsseconds, gpsmicrosec)
                    write_pcl_txt(fpath, self.timestamps, self.pos_X, self.pos_Y, self.pos_Z, self.indicies,
                              self.intensities, self.latitudes, self.longitudes, self.distances)

                if self.params['ply']:
                    fpath = "{}/{}_frame_{:06d}.pcd".format(self.pcl_path, self.frame_nr, gpsmicrosec)
                    write_pcd(fpath,index, self.pos_X, self.pos_Y, self.pos_Z, self.intensities)

                #calculate roughness and dtrack parameters and save those to txt
                #initialize arrays for roughness parameter for each roll over
                length =np.array([])
                mean_h =np.array([])
                rms_h = np.array([])
                ts_beg =np.array([])
                ts_end = np.array([])
                track_id =np.array([])
                #combine data into one data frame with pandas
                track=np.column_stack((self.timestamps,self.indicies,self.pos_X,self.pos_Y,self.pos_Z))
                dat=pd.DataFrame(track,columns=["Time [ms]", "ID", "X[m]", "Y[m]", "Z[m]"])
                #filter arrays for different beams /tracks
                """for i in range(0,32):
                    track = dat[(dat["ID"]==i)]
                    track.reset_index(drop=True, inplace=True)
                    print(track)
                    #compute track length, array ordered in ascending azimuth so first and last entry are farthest apart
                    if track.shape[0]>1:
                        track_l, h_m, h_dev =self.calc_r_para(track)
                    #append result arrays
                    length = np.append(length,track_l)
                    mean_h = np.append(mean_h, h_m)
                    rms_h = np.append(rms_h, h_dev)
                    ts_beg = np.append(ts_beg, track.iloc[0,0])
                    ts_end = np.append(ts_end, track.iloc[-1,0])
                    track_id = np.append(track_id, i)

                    #write parameter results into txt file
                    out_arr=np.column_stack((ts_beg, ts_end, track_id, length, mean_h, rms_h))
                    fpath = "{}/{}_frame_roughness.txt".format(self.txt_r_path,self.frame_nr)
                    np.savetxt(fpath, out_arr,delimiter=",", header="Timestamp begin [ms], Timestamp end [ms], ID, Track length [m], Mean hight [m], RMS Hight [m]")
                    """
                #compute roughness for only one designated scan line 
                track = dat[(dat["ID"]==15)]
                track.reset_index(drop=True, inplace=True)
                #print(track)
                #compute track length, array ordered in ascending azimuth so first and last entry are farthest apart
                if track.shape[0]>1:
                    track_l, h_m, h_dev =self.calc_r_para(track)
                
                #write parameter results into txt file
                if self.rough_fp is None:
                    try:
                        rough_path = Path("{}/{}.txt".format(self.out_path, "data_rough"))
                        self.rough_fp = open(rough_path, 'w')
                        # write roughness as a text-file
                        header = "Timestamp begin [ms], Timestamp end [ms], ID, Track length [m], Mean hight [m], RMS Hight [m]\n"
                        self.rough_fp.write(header)
                    except Exception as ex:
                        print(ex)

                txt = "{}, {}, {}, {}, {}, {}\n".format(track.iloc[0,0], track.iloc[-1,0],15,track_l,h_m,h_dev )
        
                self.rough_fp.write(txt)
    

                # reset states
                if idx_rollovr > 0:
                    self.pos_X = cur_X[idx_rollovr+1:]
                    self.pos_Y = cur_Y[idx_rollovr+1:]
                    self.pos_Z = cur_Z[idx_rollovr+1:]
                    self.intensities = cur_intensities[idx_rollovr+1:]
                    self.latitudes = cur_latitudes[idx_rollovr+1:]
                    self.timestamps = cur_timestamps[idx_rollovr+1:]
                    self.distances = cur_distances[idx_rollovr+1:]
                    self.indicies = cur_indicies[idx_rollovr+1:]
                    self.longitudes = cur_longitudes[idx_rollovr+1:]
                else:
                    self.pos_X = cur_X
                    self.pos_Y = cur_Y
                    self.pos_Z = cur_Z
                    self.intensities = cur_intensities
                    self.latitudes = cur_latitudes
                    self.timestamps = cur_timestamps
                    self.distances = cur_distances
                    self.indicies = cur_indicies
                    self.longitudes = cur_longitudes

                print(self.frame_nr)
                self.frame_nr += 1

                # reset roll over check
                self.cur_azimuth = None
                return

            self.pos_X = np.hstack((self.pos_X, cur_X))
            self.pos_Y = np.hstack((self.pos_Y, cur_Y))
            self.pos_Z = np.hstack((self.pos_Z, cur_Z))
            self.intensities = np.hstack((self.intensities, cur_intensities))
            self.latitudes = np.hstack((self.latitudes, cur_latitudes))
            self.timestamps = np.hstack((self.timestamps, cur_timestamps))
            self.distances = np.hstack((self.distances, cur_distances))
            self.indicies = np.hstack((self.indicies, cur_indicies))
            self.longitudes = np.hstack((self.longitudes, cur_longitudes))

            self.last_azimuth = cur_latitudes

        else:  
             # initilaise states
            if index == 0 or self.pos_X is None:
                self.pos_X = cur_X
                self.pos_Y = cur_Y
                self.pos_Z = cur_Z
                self.intensities = cur_distances
                self.latitudes = cur_latitudes
                self.timestamps = cur_timestamps
                self.distances = cur_distances
                self.indicies = cur_indicies
                self.longitudes = cur_longitudes


            if self.cur_azimuth is None:
                self.cur_azimuth = cur_latitudes
                self.last_azimuth = cur_latitudes

            # update current azimuth before checking for roll over
            self.cur_azimuth = cur_latitudes

            # check if a frame is finished
            idx_rollovr = self.is_roll_over()

            # handle rollover (full 360 degree frame)
            #discard any values outside FOV
            if idx_rollovr is not None:
                if idx_rollovr > 0:
                    #for i in range(0, len(cur_X)):
                    #    if (cur_latitudes[i]<=self.params["az_max"]) and (cur_latitudes[i]>= self.params["az_min"]):
                    #        self.pos_X = np.hstack((self.pos_X, cur_X[i]))
                    #        self.pos_Y = np.hstack((self.pos_Y, cur_Y[i]))
                    #        self.pos_Z = np.hstack((self.pos_Z, cur_Z[i]))
                    #        self.intensities = np.hstack((self.intensities, cur_intensities[i]))
                    #        self.latitudes = np.hstack((self.latitudes, cur_latitudes[i]))
                    #        self.timestamps = np.hstack((self.timestamps, cur_timestamps[i]))
                    #        self.distances = np.hstack((self.distances, cur_distances[i]))
                    #        self.indicies = np.hstack((self.indicies, cur_indicies[i]))
                    #        self.longitudes = np.hstack((self.longitudes, cur_longitudes[i]))


                    self.pos_X = np.hstack((self.pos_X, cur_X[0:idx_rollovr-31]))
                    self.pos_Y = np.hstack((self.pos_Y, cur_Y[0:idx_rollovr-31]))
                    self.pos_Z = np.hstack((self.pos_Z, cur_Z[0:idx_rollovr-31]))
                    self.intensities = np.hstack((self.intensities, cur_intensities[0:idx_rollovr-31]))
                    self.latitudes = np.hstack((self.latitudes, cur_latitudes[0:idx_rollovr-31]))
                    self.timestamps = np.hstack((self.timestamps, cur_timestamps[0:idx_rollovr-31]))
                    self.distances = np.hstack((self.distances, cur_distances[0:idx_rollovr-31]))
                    self.indicies = np.hstack((self.indicies, cur_indicies[0:idx_rollovr-31]))
                    self.longitudes = np.hstack((self.longitudes, cur_longitudes[0:idx_rollovr-31]))

                min, sec, micro = self.time_from_lidar(self.timestamps[0])
                self.datetime = self.datetime.replace(minute=min, second=sec, microsecond=int(micro))
                gpsweek, gpsdays, gpsseconds, gpsmicrosec = utc_to_weekseconds(self.datetime, 0)

                #fill measuremtn arrays with current frame
                erg_x=cur_X
                erg_y=cur_Y
                erg_z=cur_Z                
                erg_azimuth = self.cur_azimuth
                erg_intensities=cur_intensities
                erg_ID=cur_indicies
                erg_timestamps =cur_timestamps

                #write current frame into txt and pcd format depending on args

                if self.params['text']:
                    fpath = "{}/{}_frame_{}.{:06d}.txt".format(self.txt_path, self.frame_nr, gpsseconds, gpsmicrosec)
                    write_pcl_txt(fpath, self.timestamps, self.pos_X, self.pos_Y, self.pos_Z, self.indicies,
                              self.intensities, self.latitudes, self.longitudes, self.distances)

                if self.params['ply']:
                    fpath = "{}/{}_frame_{:06d}.pcd".format(self.pcl_path, self.frame_nr, gpsmicrosec)
                    write_pcd(fpath, index, self.pos_X, self.pos_Y, self.pos_Z, self.intensities)

                #calculate roughness and dtrack parameters and save those to txt
                #initialize arrays for roughness parameter for each roll over
                length =np.array([])
                mean_h =np.array([])
                rms_h = np.array([])
                ts_beg =np.array([])
                ts_end = np.array([])
                track_id =np.array([])
                #combine data into one data frame with pandas
                track=np.column_stack((self.timestamps,self.indicies,self.pos_X,self.pos_Y,self.pos_Z))
                dat=pd.DataFrame(track,columns=["Time [ms]", "ID", "X[m]", "Y[m]", "Z[m]"])
                #filter arrays for different beams /tracks
                #compute roughness for only one designated scan line 

                track = dat[(dat["ID"]==10)]
                track.reset_index(drop=True, inplace=True)
                #print(track)
                #compute track length, array ordered in ascending azimuth so first and last entry are farthest apart
                if track.shape[0]>1:
                    track_l, h_m, h_dev =self.calc_r_para(track)
                #write parameter results into txt file
                if self.rough_fp is None:
                    try:
                        rough_path = Path("{}/{}.txt".format(self.out_path, "data_rough"))
                        self.rough_fp = open(rough_path, 'w')
                        # write roughness as a text-file
                        header = "Timestamp begin [ms], Timestamp end [ms], ID, Track length [m], Mean hight [m], RMS Hight [m]\n"
                        self.rough_fp.write(header)
                    except Exception as ex:
                        print(ex)

                txt = "{}, {}, {}, {}, {}, {}\n".format(track.iloc[0,0], track.iloc[-1,0],10,track_l,h_m,h_dev )
        
                self.rough_fp.write(txt)

                # reset states
                if idx_rollovr > 0:
                    self.pos_X = cur_X[idx_rollovr+1:]
                    self.pos_Y = cur_Y[idx_rollovr+1:]
                    self.pos_Z = cur_Z[idx_rollovr+1:]
                    self.intensities = cur_intensities[idx_rollovr+1:]
                    self.latitudes = cur_latitudes[idx_rollovr+1:]
                    self.timestamps = cur_timestamps[idx_rollovr+1:]
                    self.distances = cur_distances[idx_rollovr+1:]
                    self.indicies = cur_indicies[idx_rollovr+1:]
                    self.longitudes = cur_longitudes[idx_rollovr+1:]
                else:
                    self.pos_X = cur_X
                    self.pos_Y = cur_Y
                    self.pos_Z = cur_Z
                    self.intensities = cur_intensities
                    self.latitudes = cur_latitudes
                    self.timestamps = cur_timestamps
                    self.distances = cur_distances
                    self.indicies = cur_indicies
                    self.longitudes = cur_longitudes
                print(self.frame_nr)
                self.frame_nr += 1

                # reset roll over check
                self.cur_azimuth = None
                return

            self.pos_X = np.hstack((self.pos_X, cur_X))
            self.pos_Y = np.hstack((self.pos_Y, cur_Y))
            self.pos_Z = np.hstack((self.pos_Z, cur_Z))
            self.intensities = np.hstack((self.intensities, cur_intensities))
            self.latitudes = np.hstack((self.latitudes, cur_latitudes))
            self.timestamps = np.hstack((self.timestamps, cur_timestamps))
            self.distances = np.hstack((self.distances, cur_distances))
            self.indicies = np.hstack((self.indicies, cur_indicies))
            self.longitudes = np.hstack((self.longitudes, cur_longitudes))

            self.last_azimuth = cur_latitudes

    #routine of position frame processing
    def process_gps_frame(self, data, timestamp, index,flag_init):
        gps_msg, imu_data, timestamp, kalman_para = self.lidar.process_position_frame(self.params, data, index,flag_init)
        #print(gps_msg.long, gps_msg.long_ori) 

        # open gps file to write in
        if self.gps_fp is None:
            try:
                gps_path = Path("{}/{}.txt".format(self.out_path, "data_gps"))
                self.gps_fp = open(gps_path, 'w')
                # write gps as a text-file
                header = "UTC-Time,Timestamp, Week, Seconds [sow], Status, Latitude [Deg], N/S, Longitudes [Deg], W/E, Velocity [m/s], Gyro1 [deg/sec], Gyro2 [deg/sec], Gyro3 [deg/sec], T1 [°C], T2 [°C], T3 [°C], Acc X1 [G], Acc X2 [G], Acc X3 [G], Acc Y1 [G], Acc Y2 [G], Acc Y3 [G]\n"
                self.gps_fp.write(header)

                kalman_path = Path("{}/{}.txt".format(self.out_path, "data_kalman"))
                self.kalman_fp = open(kalman_path,'w')
                #write kalman parameters as text-file
                header1 = "Timestamp, Roll, Pitch, Yaw, X, Y, Velocity X, Velocity Y\n"
                self.kalman_fp.write(header1)
            except Exception as ex:
                print(ex)

        txt = "{},{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}\n".format(gps_msg.datetime, timestamp, gps_msg.weeks, gps_msg.seconds,
                                                                                                              gps_msg.status, gps_msg.lat, gps_msg.lat_ori,
                                                                                                              gps_msg.long, gps_msg.long_ori, gps_msg.velocity,
                                                                                                              imu_data[0], imu_data[1], imu_data[2], imu_data[3], imu_data[4],
                                                                                                              imu_data[5], imu_data[6], imu_data[7], imu_data[8], imu_data[9],
                                                                                                              imu_data[10], imu_data[11])
        txt1 = "{},{},{},{},{},{},{},{}\n".format(kalman_para[0],kalman_para[1],kalman_para[2],kalman_para[3],kalman_para[4],kalman_para[5],kalman_para[6],kalman_para[7])

        self.gps_fp.write(txt)
        self.kalman_fp.write(txt1)

    #divide stream into frames of one full rotation
    def is_roll_over(self):
        """
        Check if one frame is completed, therefore 360 degree rotation of the lidar
        return
        """

        diff_cur = self.cur_azimuth[0:-1] - self.cur_azimuth[1:]
        diff_cur_last = self.cur_azimuth - self.last_azimuth

        res_cur = np.where(diff_cur > 0.)[0]
        res_cur_last =  np.where(diff_cur_last < 0.)[0]
        if res_cur.size > 0:
            index = res_cur[0]
            return index
        elif res_cur_last.size > 0:
            index = res_cur_last[0]
            return index
        else:
            return None

    #get precise timstamp from lidar in micro second
    def time_from_lidar(self, timestamp):
        """
        convert the timestamp [top of the hour in microsec] of a firing into
        minutes, seconds and microseconds
        :param timestamp:
        :return:
        """
        if timestamp==0.0:
            micro =0
            sec= 0
            min = 0
        else:
            micro = timestamp % (1000*1000)
            min_float = timestamp / (1000*1000*60)
            min = int(min_float)
            sec = int((min_float % min) * 60)
            min = int(min)
        return min, sec, micro

    #create output folder sturcture
    def create_folders(self):
        self.out_path = Path("{}/{}".format(self.out_root, self.lidar_type.lower()))

        # creating output dir
        try:
            os.makedirs(self.out_path.absolute())
        except Exception as ex:
            print(str(ex))
            return False

        # create point cloud dirs
        self.pcl_path = Path("{}/{}".format(self.out_path, "data_pcl"))
        try:
            os.makedirs(self.pcl_path.absolute())
        except Exception as ex:
            print(str(ex))
            return False

        # if text-files are desired, create text-file dir
        if self.params['text']:
            self.txt_path = Path("{}/{}".format(self.out_path, "data_ascii"))
            try:
                os.makedirs(self.txt_path.absolute())
            except Exception as ex:
                print(str(ex))
                return False
        # if text-files are desired, create dir for roughness in txt format
        if self.params['text']:
            self.txt_r_path = Path("{}/{}".format(self.out_path, "data_roughness"))
            try:
                os.makedirs(self.txt_r_path.absolute())
            except Exception as ex:
                print(str(ex))
                return False
        return True

#routine to write txt output file
def write_pcl_txt(path, timestamps, X, Y, Z,  laser_id, intensities=None, latitudes=None, longitudes=None, distances=None):
    header = "Time [musec], X [m], Y [m], Z [m], ID, Intensity, Azimut [Deg], Elevation [Deg], Distance [m]"
    try:
        fp = open(path, 'w')
        np.savetxt(fp, [], delimiter=', ', header=header)
    except Exception as ex:
        print(str(ex))
        return

    M = np.vstack((timestamps, X, Y, Z, laser_id))

    if intensities is not None:
        M = np.vstack((M, intensities))
    if latitudes is not None:
        M = np.vstack((M, latitudes))
    if longitudes is not None:
        M = np.vstack((M, longitudes))
    if distances is not None:
        M = np.vstack((M, distances))

    np.savetxt(fp, M.T, fmt=('%d', '%.6f', '%.6f', '%.6f', '%d', '%d', '%.3f', '%.3f', '%.3f'), delimiter=', ')
    fp.close()

#write pcd output file in binary format
def write_pcd(path, i, X, Y, Z,  intensities=None):
    template = """VERSION {}\nFIELDS {}\nSIZE {}\nTYPE {}\nCOUNT {}\nWIDTH {}\nHEIGHT {}\nVIEWPOINT {}\nPOINTS {}\nDATA {}\n"""

    #X = X.astype(np.float32).reshape(1, -1)
    #Y = Y.astype(np.float32).reshape(1, -1)
    #Z = Z.astype(np.float32).reshape(1, -1)
    norm = mpl.colors.Normalize(vmin=0, vmax=255)

    if intensities is not None:
        intensities = norm(intensities)
        col = cm.plasma(intensities)

        dtype = o3d.core.float32
        pcd = o3d.geometry.PointCloud()
        xyz = np.zeros((len(X.astype(np.float32)),3))
        xyz[:,0]=np.reshape(X.astype(np.float32),-1)
        xyz[:,1]=np.reshape(Y.astype(np.float32),-1)
        xyz[:,2]=np.reshape(Z.astype(np.float32),-1)

        rgb = np.zeros((len(col[:,0]),3))
        rgb[:,0]=np.reshape(col[:,0],-1)
        rgb[:,1]=np.reshape(col[:,1],-1)
        rgb[:,2]=np.reshape(col[:,2],-1)

        pcd.points = o3d.utility.Vector3dVector(xyz)
        pcd.colors = o3d.utility.Vector3dVector(rgb)

        #print(pcd.points)

        #fp = open(path, 'wb')
        o3d.io.write_point_cloud(path, pcd, write_ascii=False, compressed = True)
        #fp.close()
    #   I = intensities.astype(np.float32).reshape(1, -1)
    #   M = np.hstack((X.T, Y.T, Z.T, I.T))
    #    pc_data = M.view(np.dtype([('x', np.float32), ('y', np.float32), ('z', np.float32), ('i', np.float32)]))
    #    tmpl = template.format("0.7", "x y z intensity", "4 4 4 4", "F F F F", "1 1 1 1", pc_data.size, "1",
    #                           "0 0 0 1 0 0 0", pc_data.size, "binary")
    #else:
    #    M = np.hstack((X.T, Y.T, Z.T))
    #    pc_data = M.view(np.dtype([('x', np.float32), ('y', np.float32), ('z', np.float32)]))
    #    tmpl = template.format("0.7", "x y z", "4 4 4", "F F F", "1 1 1", pc_data.size, "1", "0 0 0 1 0 0 0",
    #                           pc_data.size, "binary")

    #fp = open(path, 'wb')
    #fp.write(tmpl.encode())
    #fp.write(pc_data.tostring())
    #fp.close()
