# VelodyneHDL32E Live Data Project

Python implementation of a live data read from the Velodyne-HDL-32E LiDAR sensor. The program is capable to read from pcap files and translate to txt files, and point cloud format. Furthermore it provides a live data read out in XYZ coordinates, Roughness Parameters for each scan line and saves GPS and IMU measurments of the sensor separately. 
The Project further implements an extended Kalman filter for live Position Estimation and georeferencing the Point Cloud. Orientation Estimation of the Sensor is done using an Unscented Kalman Filter (Untuned and at the moment not producing the desired results)


# Dependencies

In addtion to more frequently used packages like numpy, pandas, matplotlib etc. the following packages are used:

- Open3d
- PyProj

# Configurating

The Software Module can be configured in the params.yaml file. The following parameters can be set:

- gps: Flag whether to save GPS and IMU txt-files 
- text: Flag whether to save points and additional parameters as txt-files
- ply: Flag whether to save point clouds in pcd format
- ori_cor: Flag whether orientation correction is performed during this processing step
- pos_cor: Flag whether position correction is performed during this processing step
- from: Definition from which packet to start reading pcap file
- to: Definition which packet is the last processed from pcap file (-1 means until finished if exact number is unknown)

# Operation

The Software package can be used in the command line using: 

- -o Output folder location e.g. /Users/Name/Location/of/output/folder
- -c Configuration file location e.g.  /Users/Name/Path/to/configuration/file/params.yaml
- -t Lidar Type e.g. "VelodyneHDL32E"
- -p Path to the PCAP file e.g.
    /Users/Name/location/of/stored/pcap/file.pcap \\This parameter is only needed for the $main.py$ file where the defined PCAP file is taken as data input. 

complete command line: python main.py -o"/Users/Name/Location/of/output/folder"\\ -c"/Users/Name/Path/to/configuration/file/params.yaml" -t"VelodyneHDL32E" \\-p/Users/Name/location/of/stored/pcap/file.pcap

The software runs until interrupted manually or through an error. All outputs are written into the specified folder structure.

# Remarks

- The Orientation Correction is not operational at the Moment. It needs to be reconfigured and tuned according to the application. 
- When integrating a seperate INS or IMU a desync in packet arrival needs to be considered. Points where latitude, longitude, pitch, yaw and roll are integrated are mared in the lidar.py file
- This Software was developed during a Master Project and Thesis and is not maintained further.